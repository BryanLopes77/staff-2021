public class DwarfBarbaLonga extends Dwarf {
    private SortearNumero sortearNumero;

    public AnaoBarbaLonga(String nome) {
        super(nome);
        this.sortearNumero = new DadoD6();
    }

    public void sofrerDano() {
        boolean perdeVida = sortearNumero.sortear() <= 2;

        if(perdeVida) {
            super.sofrerDano();
        }
    }
}