import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DwarfTest {

    @Test
    public void instanciandoDwarf() {
        Dwarf dwarf = new Dwarf("Gimli");

        dwarf.getNome();

        assertEquals( "Gimli", dwarf.getNome() );
    }

    @Test
    public void verificarVida() {
        Dwarf dwarf = new Dwarf("Gimli");

        assertEquals( 110.0, dwarf.getVida(), 0.01 );
    }

    @Test
    public void verificarStatus() {
        Dwarf dwarf = new Dwarf("Gimli");

        assertEquals( Status.RECEM_CRIADO, dwarf.getStatus() );
    }
    
    @Test
    public void anaoNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        Item esperado = new Item( 1, "Escudo" );
        Item resultado = dwarf.getInventario().obterPosicao(0);
        assertEquals( esperado, resultado );
    }
    
    @Test
    public void anaoEquipadoTomaMetade() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        dwarf.equiparEDesequiparEscudo();
        dwarf.sofrerDano();
        assertEquals( 105.0, dwarf.getVida(), 0.00001 );
    }
    
    @Test
    public void anaoNaoEquipadoETomaDanoIntegral() {
        Dwarf dwarf = new Dwarf( "Gimli" );
        dwarf.sofrerDano();
        assertEquals( 100.0, dwarf.getVida(), 0.00001 );
    }
}