public class Elfo extends Personagem implements HabilidadeElfo{
    private int indiceFlecha;
    private static int qtdElfos;
    
    {
        this.indiceFlecha = 1;
        Elfo.qtdElfos = 0;
    }

    public Elfo( String nome ) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item( 1, "Arco" ));
        this.inventario.adicionar(new Item( 2, "Flecha" ));
        Elfo.qtdElfos++;
    }
    
    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }

    public Item getFlecha() {
        return this.inventario.obterPosicao(indiceFlecha);
    }

    private int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }

    private boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }
    
    @Override
    public void atirarFlecha( Dwarf inimigo ) {
        if( this.podeAtirar() ) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            super.sofrerDano();
            inimigo.sofrerDano();
        }
    }
    
    public void atirarMultiplasFlechas( int vezes, Dwarf anao ) {
         for( int i = 0; i < vezes; i++ ) {
             this.atirarFlecha(anao);
         }
     }
}