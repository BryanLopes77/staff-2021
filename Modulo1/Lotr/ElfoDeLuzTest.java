import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ElfoDeLuzTest{
    
    private static final double DELTA = 1e-1;
    
    @Test
    public void ElfoDeLuzNasceComEspada() {
        Elfo feanor = new ElfoDeLuz("Feanor");
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(1, "Arco"),
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void ElfoDeLuzDevePerderVida() {
        ElfoDeLuz feanor = new ElfoDeLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(79, feanor.getVida(), DELTA);
        assertEquals(100, gul.getVida(), DELTA);
    }

    @Test
    public void ElfoDeLuzDeveGanharVida() {
        ElfoDeLuz feanor = new ElfoDeLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89, feanor.getVida(), DELTA);
        assertEquals(90, gul.getVida(), DELTA);
    }

    @Test
    public void ElfoDeLuzNaoPodePerderEspadaDeGalvorn() {
        Elfo feanor = new ElfoDeLuz("Feanor");
        feanor.perderItem(new Item(1, "Espada de Galvorn"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(1, "Arco"),
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void ElfoDeLuzPodePerderArco() {
        Elfo feanor = new ElfoDeLuz("Feanor");
        feanor.perderItem(new Item(1, "Arco"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void ElfoDeLuzSoAtacaComUnidadeDeEspada() {
        ElfoDeLuz feanor = new ElfoDeLuz("Feanor");
        feanor.getInventario().getItens().get(2).setQuantidade(0);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(79, feanor.getVida(), DELTA);
    }
}