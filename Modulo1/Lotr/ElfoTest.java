import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoTest {

    @Test
    public void exemploMudancaValoresItem() {
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade( 1000 );
        assertEquals( 1000, flecha.getQuantidade() );
    }

    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfo = new Elfo ( "Legolas" );
        assertEquals( 2, elfo.getFlecha().getQuantidade() );
    }

    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf( "Gimli" );
        elfo.atirarFlecha( dwarf );
        assertEquals( 1, elfo.getFlecha().getQuantidade() );
        assertEquals( 1, elfo.getExperiencia() );
    }

    @Test
    public void deveGanharExperienciaApenasAoAtirarFlechasQuePossui() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf( "Gimli" );
        for( int i = 0; i < 3; i++ ) {
            elfo.atirarFlecha( dwarf );
        }
        assertEquals( 2, elfo.getExperiencia() );
        assertEquals( 0, elfo.getFlecha().getQuantidade() );
    }

    @Test
    public void deveDiminuirAVidaDoDwarfQuandoAtacado() {
        Elfo elfo = new Elfo( "Legolas" );
        Dwarf dwarf = new Dwarf( "Gimli" );
        elfo.atirarFlecha( dwarf );
        assertEquals( 100.0, dwarf.getVida(), 0.01 );
        assertEquals( Status.SOFREU_DANO, dwarf.getStatus() );
    }

    @Test
    public void elfoStatus() {
        Elfo elfo = new Elfo ( "Legolas" );
        elfo.getStatus();
        assertEquals( Status.RECEM_CRIADO, elfo.getStatus() );
    }
}    