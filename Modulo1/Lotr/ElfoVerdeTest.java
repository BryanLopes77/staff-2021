import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElfoVerdeTest{
    
    @Test
    public void elfoVerdeGanha2XpPorFlecha(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        celebron.atirarFlecha(new Dwarf("Balin"));
        assertEquals( 2, celebron.getExperiencia() );
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDecricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de vidro");
        celebron.ganharItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals( arcoDeVidro, inventario.obterPosicao(2) );
    }
    
    @Test
    public void elfoVerdeNaoAdicionaItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        celebron.ganharItem(arcoDeMadeira);
        Inventario inventario = celebron.getInventario();
        assertNull( inventario.buscar("Arco de Madeira") );
    }
    
    @Test
    public void elfoVerdePerderItemComDecricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de vidro");
        celebron.ganharItem(arcoDeVidro);
        
        Inventario inventario = celebron.getInventario();
        
        celebron.perderItem(arcoDeVidro);
        assertNull( inventario.buscar("Arco de vidro") );
    }
}
