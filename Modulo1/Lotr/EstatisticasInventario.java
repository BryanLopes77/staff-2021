public class EstatisticasInventario {
    private Inventario inventario;
    
    public EstatisticasInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
    
    private boolean estaVazio(){
        return this.inventario.getItens().isEmpty();
    }
    
    public double calcularMedia() {
        if( this.estaVazio() ) {
            return Double.NaN;
        }
        
        double somaQuantidades = 0;
        for ( Item item : this.inventario.getItens() ) {
            somaQuantidades += item.getQuantidade();
        }
        
        return somaQuantidades / inventario.getItens().size();
    }
     
    public double calcularMediana() {
        if( this.estaVazio() ) {
            return Double.NaN;
        }
        
        int  qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obterPosicao( meio ).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if( qtdImpar ) {
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obterPosicao( meio - 1 ).getQuantidade();
        return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int qtdAcima = 0;
        
        for ( Item item : this.inventario.getItens() ) {
            if( item.getQuantidade() > media ){
                qtdAcima++;
            }
        }
        
        return qtdAcima;
    }
}