import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EstatisticasInventarioTest {

    @Test
    public void calcularMediaDaQuantidadeDeItens() {
        Item pneu = new Item(2, "Pneu");
        Item oculos = new Item(4, "Oculos");
        Item relogio = new Item(3, "Relogio");
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);

        inventario.adicionar(pneu);
        inventario.adicionar(oculos);
        inventario.adicionar(relogio);
        estatisticasInventario.calcularMedia();

        assertEquals( 3, estatisticasInventario.calcularMedia(), 1e-8 );
    }

    @Test
    public void calcularMedianaQtdsPares() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas" ));
        inventario.adicionar(new Item( 20, "Adaga" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMediana();
        assertEquals( 15, resultado, 1e-8 ); //0.00000001
    }

    @Test
    public void calcularMedianaQtdsImpar() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas de gelo" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMediana();
        assertEquals( 10, resultado, 1e-8 ); //0.00000001
    }
    
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        double resultado = estatisticas.calcularMedia();
        assertTrue( Double.isNaN( resultado ) );
    }
    
    @Test
    public void qtdItensAcimaDaMediaComItensIgualMedia() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 1, "Espada de Madeira" ));
        inventario.adicionar(new Item( 2, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 3, "Botas" ));
        inventario.adicionar(new Item( 4, "Adaga" ));
        inventario.adicionar(new Item( 5, "Luva" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 2, resultado );
    }
    
    @Test
    public void qtdItensAcimaDaMediaVariosItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas de gelo" ));
        inventario.adicionar(new Item( 30, "Adaga" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 2, resultado );
    }
    
    @Test
    public void qtdItensAcimaDaMediaUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Escudo de Madeira" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 0, resultado );
    }
}