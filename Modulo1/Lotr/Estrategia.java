import java.util.ArrayList;

public interface Estrategia {
    ArrayList<Elfo> noturnosPorUltimo();
    ArrayList<Elfo> ordemDeAtaqueIntercalado();
    ArrayList<Elfo> MaioriaVerdeComFlecha();
}
