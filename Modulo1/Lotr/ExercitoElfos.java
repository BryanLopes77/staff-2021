import java.util.*;

public class ExercitoElfos implements Estrategia{
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    public void alistar( Elfo elfo ) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains( elfo.getClass() );
        
        if( podeAlistar ) {
            elfos.add( elfo );
            
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get( elfo.getStatus() );
            if( elfoDeUmStatus == null ) {
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put( elfo.getStatus(), elfoDeUmStatus );
            }
            elfoDeUmStatus.add( elfo );
        }
    }
    
    public ArrayList<Elfo> buscar( Status status ) {
        return this.porStatus.get( status );
    }
    
    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
    
    private ArrayList<Elfo> organizarElfosDoContingente(ArrayList<Elfo> contingente) {
        for(int i = 0; i < contingente.size(); i++) {
            for (int j = 0; j < contingente.size() - 1; j++) {
                Elfo elfoAtual = contingente.get(j);
                Elfo elfoProximo = contingente.get(j+1);

                if(elfoAtual instanceof ElfoNoturno) {
                    Elfo elfoTrocado = elfoAtual;
                    contingente.set(j, elfoProximo);
                    contingente.set(j + 1, elfoTrocado);
                }
            }
        }

        return contingente;
    }

    private ArrayList<Elfo> organizarPorQuantidadeDeFlechas(ArrayList<Elfo> pelotao, TipoOrdenacao ordenacao) {
        for( int i = 0; i < pelotao.size(); i++ ) {
            for( int j = 0; j < pelotao.size() - 1; j++ ) {
                Elfo atual = pelotao.get(j);
                Elfo proximo = pelotao.get(j+1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                        ( atual.getFlecha().getQuantidade() > proximo.getFlecha().getQuantidade() ) :
                        ( atual.getFlecha().getQuantidade() < proximo.getFlecha().getQuantidade() );
                if( deveTrocar ) {
                    Elfo elfoTrocado = atual;
                    pelotao.set(j, proximo);
                    pelotao.set(j + 1, elfoTrocado);
                }
            }
        }

        return pelotao;
    }

    private int verificarElfosVerdesNoExercito(ArrayList<Elfo> contingente) {
        int elfosVerdes = 0;

        for (Elfo elfo : contingente) {
            if (elfo instanceof ElfoVerde) {
                elfosVerdes++;
            }
        }

        return elfosVerdes;
    }

    private int verificarElfosNoturnosNoExercito(ArrayList<Elfo> contingente) {
        int elfosNoturnos = 0;

        for (Elfo elfo : contingente) {
            if (elfo instanceof ElfoNoturno) {
                elfosNoturnos++;
            }
        }

        return elfosNoturnos;
    }

    private ArrayList<Elfo> inserirMembrosDisponiveisNoPelotao() {
        ArrayList<Elfo> pelotao = new ArrayList<>();

        if (porStatus.get(Status.RECEM_CRIADO) != null) {
            pelotao.addAll(porStatus.get(Status.RECEM_CRIADO));
        }
        if (porStatus.get(Status.SOFREU_DANO) != null) {
            pelotao.addAll(porStatus.get(Status.SOFREU_DANO));
        }

        return pelotao;
    }

    private ArrayList<Elfo> equilibrarPelotao(ArrayList<Elfo> pelotao) {
        int numeroElfoVerde = verificarElfosVerdesNoExercito(pelotao);
        int numeroElfoNoturno = verificarElfosNoturnosNoExercito(pelotao);

        if(numeroElfoVerde != numeroElfoNoturno) {
            int diferenca = Math.abs(numeroElfoNoturno - numeroElfoVerde);

            if (numeroElfoVerde > numeroElfoNoturno) {
                for (int i = 0; i < diferenca; i++) {
                    pelotao.remove(0);
                }
            } else {
                int ultimoIndice = pelotao.size() - 1;

                for (int i = 0; i < diferenca; i++) {
                    pelotao.remove(ultimoIndice);
                    ultimoIndice = pelotao.size() - 1;
                }
            }
        }

        return pelotao;
    }

    private ArrayList<Elfo> equilibrarPelotaoMaioriaElfoVerde(ArrayList<Elfo> pelotao) {
        int numeroElfoNoturno = verificarElfosNoturnosNoExercito(pelotao);
        int balancoIdealNoturnos = (int) Math.floor(pelotao.size() * 0.3);

        while (numeroElfoNoturno > balancoIdealNoturnos) {
            pelotao.remove(0);

            numeroElfoNoturno = verificarElfosNoturnosNoExercito(pelotao);
            balancoIdealNoturnos = (int) Math.floor(pelotao.size() * 0.3);
        }

        return pelotao;
    }

    public ArrayList<Elfo> intercalarPelotaoDeAtaque(ArrayList<Elfo> pelotao) {
        ArrayList<ElfoVerde> elfosVerdes = new ArrayList<>();
        ArrayList<ElfoNoturno> elfoNoturnos = new ArrayList<>();
        ArrayList<Elfo> pelotaoIntercalado = new ArrayList<>();

        for (Elfo elfo : pelotao) {
            if (elfo instanceof ElfoVerde) {
                elfosVerdes.add((ElfoVerde) elfo);
            } else {
                elfoNoturnos.add((ElfoNoturno) elfo);
            }
        }
        int tamanhoInicial = pelotao.size();

        for (int i = 0; i < tamanhoInicial / 2; i++) {
            pelotaoIntercalado.add(elfosVerdes.get(i));
            pelotaoIntercalado.add(elfoNoturnos.get(i));
        }

        return pelotaoIntercalado;
    }

    public ArrayList<Elfo> verificarPosseDeFlechas(ArrayList<Elfo> pelotao) {
        ArrayList<Elfo> pelotaoComFlecha = new ArrayList<>();

        for(Elfo elfo : pelotao) {
            if(elfo.getFlecha().getQuantidade() > 0) {
                pelotaoComFlecha.add(elfo);
            }
        }

        return pelotaoComFlecha;
    }

    @Override
    public ArrayList<Elfo> noturnosPorUltimo() {
        ArrayList<Elfo> elfosAtacantes = inserirMembrosDisponiveisNoPelotao();
        organizarElfosDoContingente(elfosAtacantes);

        return elfosAtacantes;
    }

    @Override
    public ArrayList<Elfo> ordemDeAtaqueIntercalado() {
        ArrayList<Elfo> elfosAtacantes = inserirMembrosDisponiveisNoPelotao();

        organizarElfosDoContingente(elfosAtacantes);
        equilibrarPelotao(elfosAtacantes);

        return intercalarPelotaoDeAtaque(elfosAtacantes);
    }

    @Override
    public ArrayList<Elfo> MaioriaVerdeComFlecha() {
        ArrayList<Elfo> elfosAtacantes = inserirMembrosDisponiveisNoPelotao();

        verificarPosseDeFlechas(elfosAtacantes);
        equilibrarPelotaoMaioriaElfoVerde(elfosAtacantes);

        return organizarPorQuantidadeDeFlechas(elfosAtacantes, TipoOrdenacao.DESC);
    }

    public void atacarAnaoFraco(ArrayList<Elfo> pelotaoDeAtaque, Dwarf dwarfFraco) {

        for (Elfo elfo : pelotaoDeAtaque) {
            elfo.atirarFlecha(dwarfFraco);
        }
    }
}
