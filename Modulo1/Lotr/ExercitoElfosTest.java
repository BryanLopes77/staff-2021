import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;

public class ExercitoElfosTest{
    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }
    
    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }
    
    @Test
    public void naoPodeAlistarElfoDeLuz() {
        Elfo elfoLuz = new ElfoDeLuz("Light Legolas");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoLuz);
        assertFalse(exercito.getElfos().contains(elfoLuz));
    }
    
    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoNoturno);
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(elfoNoturno)
        );
        assertEquals(esperado, exercito.buscar(Status.RECEM_CRIADO));
    }
    
    @Test
    public void organizarContingente() {
        ExercitoElfos exercitoDeElfos = new ExercitoElfos();
        for(int i = 0; i < 10; i++) {
            if(i >= 5) {
                exercitoDeElfos.alistar(new ElfoVerde("Elfo Verde " + i));
            } else {
                exercitoDeElfos.alistar(new ElfoNoturno("Elfo Noturno " + i));
            }
        }
        ArrayList<Elfo> exercito = exercitoDeElfos.ordemDeAtaqueIntercalado();
        
        assertTrue(exercito.get(0) instanceof ElfoVerde);
        assertTrue(exercito.get(1) instanceof ElfoNoturno);
    }
    
    @Test
    public void atacarAnaoComNoturnosPorUltimo() {
        ExercitoElfos exercitoDeElfos = new ExercitoElfos();
        for(int i = 0; i < 10; i++) {
            if(i >= 5) {
                exercitoDeElfos.alistar(new ElfoVerde("Elfo Verde " + i));
            } else {
                exercitoDeElfos.alistar(new ElfoNoturno("Elfo Noturno " + i));
            }
        }
        ArrayList<Elfo> exercito = exercitoDeElfos.noturnosPorUltimo();

        assertTrue(exercito.get(9) instanceof ElfoNoturno);
    }
    
    @Test
    public void atacarAnaoIntercalandoOsAtaques() {
        ExercitoElfos exercitoDeElfos = new ExercitoElfos();

        for(int i = 0; i < 10; i++) {
            if(i >= 5) {
                exercitoDeElfos.alistar(new ElfoVerde("Elfo Verde " + i));
            } else {
                exercitoDeElfos.alistar(new ElfoNoturno("Elfo Noturno " + i));
            }
        }

        ArrayList<Elfo> exercito = exercitoDeElfos.ordemDeAtaqueIntercalado();

        assertTrue(exercito.get(0) instanceof ElfoVerde);
        assertTrue(exercito.get(1) instanceof ElfoNoturno);
    }
}
