import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

public class InventarioTest {

    @Test
    public void adicionarItemEVerificarInventario() {
        Item pneu = new Item(2, "Pneu");
        Item oculos = new Item(2, "Oculos");
        Item relogio = new Item(2, "Relogio");
        Inventario inventario = new Inventario();

        inventario.adicionar( pneu );
        inventario.adicionar( oculos );
        inventario.adicionar( relogio );
        inventario.obterPosicao( 0 );
        inventario.obterPosicao( 1 );
        inventario.obterPosicao( 2 );
        inventario.getDescricoesItens();

        assertEquals( pneu, inventario.obterPosicao( 0 ));
        assertEquals( relogio, inventario.obterPosicao( 2 ));
    }

    @Test
    public void retornaItensDaArray() {
        Inventario inventario = new Inventario();
        Item chackra = new Item(10, "Chackra");
        Item taijutsu = new Item(8, "Taijutsu");
        Item sabre = new Item(11, "Sabre");
        Item tenis = new Item(31, "Tenis");
        Item jordan = new Item(23, "Jordan");
        inventario.adicionar(chackra);
        inventario.adicionar(taijutsu);
        inventario.adicionar(sabre);
        inventario.adicionar(tenis);
        inventario.adicionar(jordan);
        inventario.maiorQuantidade();
        ArrayList<Item> resultado = inventario.inverter();


        assertEquals( jordan, resultado.get(0) );
        assertEquals( chackra, inventario.getItens().get(0) );
    }

    @Test
    public void removerItem() {
        Item pneu = new Item(2, "Pneu");
        Item oculos = new Item(2, "Oculos");
        Item relogio = new Item(2, "Relogio");
        Inventario inventario = new Inventario();

        inventario.adicionar( pneu );
        inventario.adicionar( oculos );
        inventario.adicionar( relogio );
        inventario.remover( relogio );
        inventario.getDescricoesItens();

        assertEquals( pneu, inventario.obterPosicao( 0 ));
    }
}
        
        