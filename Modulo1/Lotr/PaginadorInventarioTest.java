import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.*;

public class PaginadorInventarioTest {
    
    @Test
    public void pularLimitarUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item( 1, "Espada" );
        inventario.adicionar( espada );
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals( espada, primeiraPagina.get(0) );
        assertEquals( 1, primeiraPagina.size() );
    }
    
    @Test
    public void pularLimitarTresItens() {
        Inventario inventario = new Inventario();
        Item sabre = new Item( 1, "Sabre" );
        Item escudo = new Item( 2, "Escudo" );
        Item espada = new Item( 10, "Espada" );
        inventario.adicionar( sabre );
        inventario.adicionar( escudo );
        inventario.adicionar( espada );
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1);
        
        assertEquals( sabre, primeiraPagina.get(0) );
        assertEquals( escudo, primeiraPagina.get(1) );
        assertEquals( 2, primeiraPagina.size() );
        
        assertEquals( espada, segundaPagina.get(0) );
        assertEquals( 1, segundaPagina.size() );
    }
    
    @Test
    public void pularLimitarForaDosLimites() {
        Inventario inventario = new Inventario();
        Item sabre = new Item( 1, "Sabre" );
        Item escudo = new Item( 2, "Escudo" );
        Item espada = new Item( 10, "Espada" );
        inventario.adicionar( sabre );
        inventario.adicionar( escudo );
        inventario.adicionar( espada );
        PaginadorInventario paginador = new PaginadorInventario( inventario );
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1000);
        
        assertEquals( sabre, primeiraPagina.get(0) );
        assertEquals( escudo, primeiraPagina.get(1) );
        assertEquals( 2, primeiraPagina.size() );
        
        assertEquals( espada, segundaPagina.get(0) );
        assertEquals( 1, segundaPagina.size() );
    }
}

