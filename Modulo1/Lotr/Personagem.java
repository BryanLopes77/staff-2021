public class Personagem {
    protected String nome;
    protected int experiencia;
    protected int qtdExperienciaPorAtaque;
    protected Inventario inventario;
    protected double vida;
    protected double qtdDano;
    protected Status status;
    
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario();
        this.experiencia = 0;
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar( item );
    }
    
    public void perderItem( Item item ) {
        this.inventario.remover( item );
    }
    
    protected void aumentarXP() {
        this.experiencia += qtdExperienciaPorAtaque;
    }
    
    private boolean podeSofrerDano( ) {
        return this.vida > 0;
    }
    
    private Status validacaoStatus() {
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    protected void sofrerDano() {
        if( this.podeSofrerDano() ) {
            this.vida -= this.vida >= this.qtdDano ? this.qtdDano : 0;
            this.status = this.validacaoStatus();
        }
        
    }
}
