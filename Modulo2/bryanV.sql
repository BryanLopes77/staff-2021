CREATE TABLE USUARIO(
    CPF char(14) PRIMARY KEY NOT NULL,
    NOME varchar2(100) NOT NULL,
    LOGIN varchar2(15) UNIQUE NOT NULL,
    SENHA varchar2(20) NOT NULL
);

CREATE TABLE STATUS(
    ID_STATUS INT NOT NULL,
    DESCRICAO VARCHAR2(20) UNIQUE NOT NULL,
    CONSTRAINT PK_ID_STATUS PRIMARY KEY ( ID_STATUS )
);

CREATE TABLE ITEM(
    ID_ITEM INT NOT NULL,
    DESCRICAO VARCHAR2(20) UNIQUE NOT NULL,
    CONSTRAINT PK_ID_ITEM PRIMARY KEY ( ID_ITEM )  
);

CREATE TABLE INVENTARIO(
    ID_INVENTARIO INT NOT NULL,
    CONSTRAINT PK_ID_INVENTARIO PRIMARY KEY ( ID_INVENTARIO )
);

CREATE TABLE ITEM_X_INVENTARIO(
    ID_ITEM INT NOT NULL,
    ID_INVENTARIO INT NOT NULL,
    QUANTIDADE INT DEFAULT 0 NOT NULL,
    CONSTRAINT PK_ID_ITEM_X_INVENTARIO PRIMARY KEY ( ID_ITEM, ID_INVENTARIO ),
    CONSTRAINT FK_ID_ITEM FOREIGN KEY ( ID_ITEM ) REFERENCES ITEM( ID_ITEM ),
    CONSTRAINT FK_ID_INVENTARIO FOREIGN KEY ( ID_INVENTARIO ) REFERENCES INVENTARIO( ID_INVENTARIO )
);

CREATE TABLE TIPO_PERSONAGEM(
    ID_TIPO_PERSONAGEM INT PRIMARY KEY NOT NULL,
    DESCRICAO VARCHAR2(20) UNIQUE NOT NULL
);

CREATE TABLE PERSONAGEM(
    ID_PERSONAGEM INT PRIMARY KEY NOT NULL,
    NOME VARCHAR2(20) UNIQUE NOT NULL,
    EXPERIENCIA INT DEFAULT 0 NOT NULL,
    VIDA DECIMAL(4,2) NOT NULL,
    QTD_DANO DECIMAL(4,2) DEFAULT 0 NOT NULL,
    QTD_EXPERIENCIA_POR_ATAQUE INT DEFAULT 1 NOT NULL,
    FK_STATUS INT NOT NULL,
    FK_INVENTARIO INT NOT NULL,
    FK_TIPO_PERSONAGEM INT NOT NULL,
    CONSTRAINT FK_STATUS_ID FOREIGN KEY ( FK_STATUS ) REFERENCES STATUS( ID_STATUS ),
    CONSTRAINT FK_INVENTARIO_ID FOREIGN KEY ( FK_INVENTARIO ) REFERENCES INVENTARIO( ID_INVENTARIO ),
    CONSTRAINT FK_TIPO_PERSONAGEM_ID FOREIGN KEY ( FK_TIPO_PERSONAGEM ) REFERENCES TIPO_PERSONAGEM( ID_TIPO_PERSONAGEM )
);

-- DROP TABLE PERSONAGEM;


CREATE TABLE BANCO(
    CODIGO CHAR(4) PRIMARY KEY NOT NULL,
    NOME VARCHAR2(50) UNIQUE NOT NULL
);

CREATE TABLE PAIS(
    ID_PAIS INT PRIMARY KEY NOT NULL,
    NOME VARCHAR2(50) UNIQUE NOT NULL
);

CREATE TABLE ESTADO(
    ID_ESTADO INT PRIMARY KEY NOT NULL,
    ID_PAIS INT NOT NULL,
    NOME VARCHAR2(50) UNIQUE NOT NULL,
    CONSTRAINT FK_ID_PAIS FOREIGN KEY ( ID_PAIS ) REFERENCES PAIS( ID_PAIS )
);

CREATE TABLE CIDADE(
    ID_CIDADE INT NOT NULL,
    ID_ESTADO INT NOT NULL,
    NOME VARCHAR2(50) NOT NULL,
    CONSTRAINT PK_ID_CIDADE_ESTADO PRIMARY KEY ( ID_CIDADE, ID_ESTADO ),
    CONSTRAINT FK_ID_ESTADO FOREIGN KEY ( ID_ESTADO ) REFERENCES ESTADO( ID_ESTADO )
);

CREATE TABLE BAIRRO(
    ID_BAIRRO INT NOT NULL,
    ID_CIDADE INT NOT NULL,
    ID_ESTADO INT NOT NULL,
    NOME VARCHAR2(50) NOT NULL,
    CONSTRAINT PK_ID_BAIRRO_CIDADE PRIMARY KEY ( ID_BAIRRO, ID_CIDADE ),
    CONSTRAINT FK_ID_CIDADE_ESTADO FOREIGN KEY ( ID_CIDADE, ID_ESTADO ) REFERENCES CIDADE( ID_CIDADE, ID_ESTADO )
);

CREATE TABLE ENDERECO(
    ID_ENDERECO INT PRIMARY KEY NOT NULL,
    ID_BAIRRO INT NOT NULL,
    ID_CIDADE INT NOT NULL,
    LOGRADOURO VARCHAR2(255) NOT NULL,
    NUMERO INT NOT NULL,
    COMPLEMENTO VARCHAR2(100) NULL,
    CEP CHAR(9) NULL,
    CONSTRAINT FK_ID_BAIRRO_CIDADE FOREIGN KEY ( ID_BAIRRO, ID_CIDADE ) REFERENCES BAIRRO( ID_BAIRRO, ID_CIDADE )
);

CREATE TABLE AGENCIA(
    CODIGO CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    ID_ENDERECO INT NOT NULL,
    NOME VARCHAR2(100) NOT NULL,
    CONSTRAINT PK_CODIGO_ID_BANCO PRIMARY KEY ( CODIGO, ID_BANCO ),
    CONSTRAINT FK_ID_BANCO FOREIGN KEY ( ID_BANCO ) REFERENCES BANCO( CODIGO ),
    CONSTRAINT FK_ID_ENDERECO FOREIGN KEY ( ID_ENDERECO ) REFERENCES ENDERECO( ID_ENDERECO )
);

CREATE TABLE CONSOLIDACAO(
    ID_CONSOLIDACAO INT PRIMARY KEY NOT NULL,
    FK_CODIGO CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    SALDO_ATUAL DECIMAL(18,2) NOT NULL,
    SAQUES DECIMAL(18,2) NOT NULL,
    DEPOSITOS DECIMAL(18,2) NOT NULL,
    NUMERO_CORRENTISTA INT NOT NULL,
    CONSTRAINT FK_CODIGO_ID_BANCO FOREIGN KEY ( FK_CODIGO, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO )
);

CREATE TABLE TIPO_CONTA(
    ID_TIPO_CONTA INT PRIMARY KEY NOT NULL,
    NOME VARCHAR2(20) NOT NULL
);

CREATE TABLE CONTA(
    CODIGO INT NOT NULL,
    CODIGO_AGENCIA CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    TIPO_CONTA INT NOT NULL,
    SALDO DECIMAL(18,2) DEFAULT 0.0 NOT NULL,
    CONSTRAINT PK_CODIGO PRIMARY KEY ( CODIGO ),
    CONSTRAINT FK_TIPO_CONTA FOREIGN KEY ( TIPO_CONTA ) REFERENCES TIPO_CONTA( ID_TIPO_CONTA ),
    CONSTRAINT FK_CODIGO_AGENCIA FOREIGN KEY ( CODIGO_AGENCIA, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO )
);

CREATE TABLE MOVIMENTACOES(
    ID_MOVIMENTACOES INT PRIMARY KEY NOT NULL,
    CODIGO INT NOT NULL,
    TIPO VARCHAR2(10) CHECK( TIPO IN ('DÉBITO', 'CRÉDITO') ),
    VALOR DECIMAL(18,2) NOT NULL,
    CONSTRAINT FK_CODIGO_CONTA FOREIGN KEY ( CODIGO ) REFERENCES CONTA( CODIGO )
);

CREATE TABLE CLIENTE(
    CPF CHAR(14) PRIMARY KEY NOT NULL,
    NOME VARCHAR2(100) NOT NULL,
    DATA_NASCIMENTO CHAR(10) NOT NULL,
    ESTADO_CIVIL VARCHAR2(20) CHECK( ESTADO_CIVIL IN ('SOLTEIRX', 'CASADX', 'DIVORCIADX', 'UNIÃO ESTÁVEL', 'VIUVX') )
);

CREATE TABLE CLIENTE_X_ENDERECO(
    ID_CLIENTE CHAR(14) NOT NULL,
    ID_ENDERECO INT NOT NULL,
    CONSTRAINT PK_ID_CLIENTE_ENDERECO PRIMARY KEY ( ID_CLIENTE, ID_ENDERECO ),
    CONSTRAINT FK_ID_ENDERECO_2 FOREIGN KEY ( ID_ENDERECO ) REFERENCES ENDERECO( ID_ENDERECO ),
    CONSTRAINT FK_ID_CLIENTE FOREIGN KEY ( ID_CLIENTE ) REFERENCES CLIENTE( CPF )
);

CREATE TABLE CLIENTE_X_CONTA(
    ID_CLIENTE CHAR(14) NOT NULL,
    CODIGO INT NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    TIPO_CONTA INT NOT NULL,
    CONSTRAINT PK_CLIENTE_BANCO_TIPO PRIMARY KEY ( ID_CLIENTE, ID_BANCO, TIPO_CONTA ),
    CONSTRAINT FK_ID_BANCO_2 FOREIGN KEY ( ID_BANCO ) REFERENCES BANCO( CODIGO ),
    CONSTRAINT FK_CODIGO_CONTA_2 FOREIGN KEY ( CODIGO ) REFERENCES CONTA( CODIGO ),
    CONSTRAINT FK_TIPO_CONTA_2 FOREIGN KEY ( TIPO_CONTA ) REFERENCES TIPO_CONTA( ID_TIPO_CONTA ),
    CONSTRAINT FK_ID_CLIENTE_2 FOREIGN KEY ( ID_CLIENTE ) REFERENCES CLIENTE( CPF )
);

CREATE TABLE AGENCIA_X_CONTA(
    ID_AGENCIA CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    ID_CONTA INT NOT NULL,
    CONSTRAINT PK_ID_AGENCIA_ID_CONTA PRIMARY KEY ( ID_AGENCIA, ID_CONTA ),
    CONSTRAINT FK_ID_AGENCIA_3 FOREIGN KEY ( ID_AGENCIA, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO ),
    CONSTRAINT FK_ID_CONTA_3 FOREIGN KEY ( ID_CONTA ) REFERENCES CONTA( CODIGO )
);

CREATE TABLE GERENTE(
    CPF CHAR(14) PRIMARY KEY NOT NULL,
    FK_CODIGO CHAR(6) NOT NULL,
    ID_BANCO CHAR(4) NOT NULL,
    CODIGO_FUNCIONARIO INT NOT NULL,
    TIPO_GERENTE CHAR(2) CHECK( TIPO_GERENTE IN ('GC', 'GG') ), 
    CONSTRAINT FK_ID_CLIENTE_3 FOREIGN KEY ( CPF ) REFERENCES CLIENTE( CPF ),
    CONSTRAINT FK_CODIGO_ID_BANCO_2 FOREIGN KEY ( FK_CODIGO, ID_BANCO ) REFERENCES AGENCIA( CODIGO, ID_BANCO )
);


CREATE TABLE GERENTE_X_CONTA(
    CPF CHAR(14) NOT NULL,
    CODIGO INT NOT NULL,
    CONSTRAINT PK_CPF_CODIGO_CONTA PRIMARY KEY ( CPF, CODIGO ),
    CONSTRAINT FK_CODIGO_CONTA_3 FOREIGN KEY ( CODIGO ) REFERENCES CONTA( CODIGO ),
    CONSTRAINT FK_ID_CLIENTE_4 FOREIGN KEY ( CPF ) REFERENCES CLIENTE( CPF )
);

CREATE TABLE BANCO_X_PAIS(
    ID_BANCO CHAR(4) NOT NULL,
    ID_PAIS INT NOT NULL,
    CONSTRAINT PK_ID_BANCO_PAIS PRIMARY KEY ( ID_BANCO, ID_PAIS ),
    CONSTRAINT FK_ID_BANCO_5 FOREIGN KEY ( ID_BANCO ) REFERENCES BANCO( CODIGO ),
    CONSTRAINT FK_ID_PAIS_5 FOREIGN KEY ( ID_PAIS ) REFERENCES PAIS( ID_PAIS )
);

INSERT INTO BANCO (CODIGO, NOME) VALUES ( '011', 'BANCO ALFA' );
INSERT INTO PAIS (ID_PAIS, NOME) VALUES ( 1, 'BRASIL');
INSERT INTO BANCO_X_PAIS(ID_BANCO, ID_PAIS) VALUES ( '011', 1 );
INSERT INTO ESTADO (ID_ESTADO, ID_PAIS, NOME) VALUES ( 1, 1, 'NA' );
INSERT INTO CIDADE (ID_CIDADE, ID_ESTADO, NOME) VALUES ( 1, 1, 'NA' );
INSERT INTO BAIRRO (ID_BAIRRO, ID_CIDADE, ID_ESTADO, NOME) VALUES ( 1, 1, 1, 'NA' );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO, COMPLEMENTO) VALUES ( 1, 1, 1, 'RUA TESTANDO', 55, 'LOJA 1' );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0001', '011', 1, 'WEB' );

INSERT INTO PAIS (ID_PAIS, NOME) VALUES ( 2, 'EUA');
INSERT INTO BANCO_X_PAIS(ID_BANCO, ID_PAIS) VALUES ( '011', 2 );
INSERT INTO ESTADO (ID_ESTADO, ID_PAIS, NOME) VALUES ( 2, 2, 'CALIFORNIA' );
INSERT INTO CIDADE (ID_CIDADE, ID_ESTADO, NOME) VALUES ( 2, 2, 'SAN FRANCISCO' );
INSERT INTO BAIRRO (ID_BAIRRO, ID_CIDADE, ID_ESTADO, NOME) VALUES ( 2, 2, 2, 'BETWEEN HYDE AND POWELL STREETS' );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 2, 2, 2, 'RUA TESTING', 122 );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0002', '011', 2, 'CALIFORNIA' );

INSERT INTO PAIS (ID_PAIS, NOME) VALUES ( 3, 'ENGLAND');
INSERT INTO BANCO_X_PAIS(ID_BANCO, ID_PAIS) VALUES ( '011', 3 );
INSERT INTO ESTADO (ID_ESTADO, ID_PAIS, NOME) VALUES ( 3, 3, 'BOROUGHS' );
INSERT INTO CIDADE (ID_CIDADE, ID_ESTADO, NOME) VALUES ( 3, 3, 'LONDRES' );
INSERT INTO BAIRRO (ID_BAIRRO, ID_CIDADE, ID_ESTADO, NOME) VALUES ( 3, 3, 3, 'CROYDON' );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 3, 3, 3, 'RUA TESING', 525 );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0101', '011', 3, 'LONDRES' );

INSERT INTO BANCO (CODIGO, NOME) VALUES ( '241', 'BANCO BETA' );
INSERT INTO BANCO_X_PAIS(ID_BANCO, ID_PAIS) VALUES ( '241', 1 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO, COMPLEMENTO) VALUES ( 4, 1, 1, 'RUA TESTANDO', 55, 'LOJA 2' );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0001', '241', 4, 'WEB' );

INSERT INTO BANCO (CODIGO, NOME) VALUES ( '307', 'BANCO OMEGA' );
INSERT INTO BANCO_X_PAIS(ID_BANCO, ID_PAIS) VALUES ( '307', 1 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO, COMPLEMENTO) VALUES ( 5, 1, 1, 'RUA TESTANDO', 55, 'LOJA 3' );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '0001', '307', 5, 'WEB' );

INSERT INTO ESTADO (ID_ESTADO, ID_PAIS, NOME) VALUES ( 4, 1, 'SÃO PAULO' );
INSERT INTO CIDADE (ID_CIDADE, ID_ESTADO, NOME) VALUES ( 4, 4, 'ITU' );
INSERT INTO BAIRRO (ID_BAIRRO, ID_CIDADE, ID_ESTADO, NOME) VALUES ( 4, 4, 4, 'QUALQUER' );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 6, 4, 4, 'RUA DO MEIO', 2233 );
INSERT INTO AGENCIA( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '8761', '307', 6, 'ITU' );

INSERT INTO PAIS (ID_PAIS, NOME) VALUES ( 4, 'ARGENTINA');
INSERT INTO BANCO_X_PAIS(ID_BANCO, ID_PAIS) VALUES ( '307', 4 );
INSERT INTO ESTADO (ID_ESTADO, ID_PAIS, NOME) VALUES ( 5, 4, 'BUENOS AIRES' );
INSERT INTO CIDADE (ID_CIDADE, ID_ESTADO, NOME) VALUES ( 5, 5, 'BUENOS AIRES' );
INSERT INTO BAIRRO (ID_BAIRRO, ID_CIDADE, ID_ESTADO, NOME) VALUES ( 5, 5, 5, 'CAMINITO' );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 7, 5, 5, 'RUA DO BOCA', 222 );
INSERT INTO AGENCIA ( CODIGO, ID_BANCO, ID_ENDERECO, NOME ) VALUES ( '4567', '307', 7, 'HERMANA' );

INSERT INTO TIPO_CONTA ( ID_TIPO_CONTA, NOME ) VALUES ( 1, 'PJ' );
INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 777, '0001', '011', 1, 2400.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 8, 1, 1, 'RUA DO BRASIL', 77 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 12, 1, 1, 'RUA DO BRASIL', 55 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 13, 1, 1, 'RUA DO BRASIL', 22 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '888.858.999-04', 'LUCAS BIN', '10/11/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '111.858.999-04', 'CARLOS BIN', '10/01/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '222.858.999-04', 'MAURICIO BIN', '10/02/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '888.858.999-04', 8 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '111.858.999-04', 12 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '222.858.999-04', 13 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '888.858.999-04', 777, '011', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '111.858.999-04', 777, '011', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '222.858.999-04', 777, '011', 1 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '011', 777 );

INSERT INTO TIPO_CONTA ( ID_TIPO_CONTA, NOME ) VALUES ( 3, 'PF' );
INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 656, '0001', '011', 3, 3300.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 14, 1, 1, 'RUA DO BRASIL CS', 44 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '888.333.999-04', 'WELLIN JR', '10/07/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '888.333.999-04', 14 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '888.333.999-04', 656, '011', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '011', 656 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 666, '0001', '011', 1, 2900.00 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 9, 1, 1, 'RUA ARLINDO CRUZ', 97 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '888.888.888-04', 'PEDRO CAUTELA', '06/11/1990', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '888.888.888-04', 9 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '888.888.888-04', 666, '011', 1 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '011', 666 );

INSERT INTO TIPO_CONTA ( ID_TIPO_CONTA, NOME ) VALUES ( 2, 'CONJ' );
INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 235, '0002', '011', 2, 11000.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 10, 1, 1, 'RUA SÃO FRANCISCO', 07 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '811.858.999-04', 'BRYAN LOPES', '10/11/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '811.858.999-04', 10 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '811.858.999-04', 235, '011', 2 );

INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '811.111.999-04', 'TAMIRES LOCKS', '10/10/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '811.111.999-04', 10 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '811.111.999-04', 235, '011', 2 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0002', '011', 235 );


INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 555, '0101', '011', 2, 211000.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 11, 1, 1, 'RUA SÃO FRANCISCO', 90 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '822.858.999-04', 'SOPHIA CURRY', '27/11/1987', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '822.858.999-04', 11 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '822.858.999-04', 555, '011', 2 );

INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '012.858.999-04', 'MARCOS CURRY', '27/08/1982', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '012.858.999-04', 11 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '012.858.999-04', 555, '011', 2 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0101', '011', 555 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 585, '0101', '011', 1, 982400.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 15, 1, 1, 'RUA DA DOCA', 88 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 16, 1, 1, 'RUA DA DOCA', 55 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 17, 1, 1, 'RUA DA DOCA', 22 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '888.858.111-04', 'LINDA JIN', '05/11/1989', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '111.858.111-04', 'FERLA JIN', '10/02/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '222.858.111-04', 'LILI JIN', '10/08/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '888.858.111-04', 15 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '111.858.111-04', 16 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '222.858.111-04', 17 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '888.858.111-04', 585, '011', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '111.858.111-04', 585, '011', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '222.858.111-04', 585, '011', 1 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0101', '011', 585 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 999, '0002', '011', 3, 11100.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 18, 1, 1, 'RUA SÃO FRANCISCO', 44 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '555.858.999-04', 'BRYAN OCONNOR', '14/06/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '555.858.999-04', 18 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '555.858.999-04', 999, '011', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0002', '011', 999 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 887, '0002', '011', 3, 1200.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 19, 1, 1, 'RUA SÃO LUCAS', 34 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '555.444.999-04', 'LUCAS GIN', '12/06/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '555.444.999-04', 19 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '555.444.999-04', 887, '011', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0002', '011', 887 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 445, '0001', '241', 2, 11000.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 20, 2, 2, 'RUA USA', 09 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '811.858.999-12', 'JULIANA LOPES', '10/11/1978', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '811.858.999-12', 20 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '811.858.999-12', 445, '241', 2 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '241', 445 );

INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '811.111.111-04', 'INDIA LOCKS', '10/01/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '811.111.111-04', 20 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '811.111.111-04', 445, '011', 2 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 555, '0001', '241', 1, 992400.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 21, 3, 3, 'RUA DA IN', 88 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 22, 3, 3, 'RUA DA IN', 55 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 23, 3, 3, 'RUA DA IN', 22 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '777.858.111-04', 'PEDRO PE', '05/11/1989', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '333.858.111-04', 'NATALIA PE', '10/02/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '444.858.111-04', 'PETER PE', '10/08/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '777.858.111-04', 21 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '333.858.111-04', 22 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '444.858.111-04', 23 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '777.858.111-04', 555, '011', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '333.858.111-04', 555, '011', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '444.858.111-04', 555, '011', 1 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '241', 555 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 119, '0001', '241', 3, 1100.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 24, 1, 1, 'RUA SÃO LINCOR', 44 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '009.858.999-04', 'LICOLN OCONNOR', '18/06/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '009.858.999-04', 24 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '009.858.999-04', 119, '241', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '241', 119 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 120, '0001', '241', 3, 1700.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 25, 1, 1, 'RUA SÃO LINCOR', 09 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '019.858.999-04', 'LICOLN OCONNOR', '18/09/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '019.858.999-04', 25 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '019.858.999-04', 120, '241', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '241', 120 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 123, '0001', '307', 1, 982400.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 26, 3, 3, 'RUA DA PINAIA', 88 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 27, 3, 3, 'RUA DA PINAIA', 55 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 28, 3, 3, 'RUA DA PINAIA', 22 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '888.858.111-77', 'GABRIEL ARC', '08/11/1989', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '111.858.111-88', 'FERGUS ARC', '01/02/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '222.858.111-99', 'CASTIEL ARC', '02/08/1999', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '888.858.111-77', 26 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '111.858.111-88', 27 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '222.858.111-99', 28 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '888.858.111-77', 123, '307', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '111.858.111-88', 123, '307', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '222.858.111-99', 123, '307', 1 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '0001', '307', 123 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 122, '8761', '307', 1, 992400.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 29, 2, 2, 'RUA DA DIN', 88 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 30, 2, 2, 'RUA DA DIN', 55 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 31, 2, 2, 'RUA DA DIN', 22 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '767.858.111-77', 'VIRJ ARC', '08/11/1983', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '767.858.111-88', 'OZIL ARC', '01/02/1992', 'SOLTEIRX' );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '767.858.111-99', 'JUAN ARC', '02/08/1991', 'SOLTEIRX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '767.858.111-77', 29 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '767.858.111-88', 30 );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '767.858.111-99', 31 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '767.858.111-77', 122, '307', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '767.858.111-88', 122, '307', 1 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '767.858.111-99', 122, '307', 1 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '8761', '307', 122 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 888, '4567', '307', 3, 1500.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 32, 3, 3, 'RUA SÃO ENGLAND', 44 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '141.444.999-04', 'LUCAS VINI', '12/06/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '141.444.999-04', 32 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '141.444.999-04', 888, '307', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '4567', '307', 888 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 897, '4567', '307', 3, 1200.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 33, 1, 1, 'RUA MT LUCAS', 34 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '555.444.999-12', 'PETER GIN', '12/06/1990', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '555.444.999-12', 33 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '555.444.999-12', 897, '011', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '4567', '307', 897 );

INSERT INTO CONTA ( CODIGO, CODIGO_AGENCIA, ID_BANCO, TIPO_CONTA, SALDO ) VALUES ( 187, '8761', '307', 3, 1200.50 );
INSERT INTO ENDERECO (ID_ENDERECO, ID_BAIRRO, ID_CIDADE, LOGRADOURO, NUMERO ) VALUES ( 34, 2, 2, 'RUA JR LUCAS', 74 );
INSERT INTO CLIENTE ( CPF, NOME, DATA_NASCIMENTO, ESTADO_CIVIL ) VALUES ( '666.444.999-04', 'CAIO JR', '21/06/2000', 'CASADX' );
INSERT INTO CLIENTE_X_ENDERECO ( ID_CLIENTE, ID_ENDERECO ) VALUES ( '666.444.999-04', 34 );
INSERT INTO CLIENTE_X_CONTA ( ID_CLIENTE, CODIGO, ID_BANCO, TIPO_CONTA ) VALUES ( '666.444.999-04', 187, '307', 3 );
INSERT INTO AGENCIA_X_CONTA( ID_AGENCIA, ID_BANCO, ID_CONTA ) VALUES ( '8761', '307', 187 );

SELECT PAIS.NOME AS PAIS,
       BB.CODIGO,
       BB.NOME AS BANCO
FROM BANCO BB
INNER JOIN AGENCIA AG
    ON BB.CODIGO = AG.ID_BANCO
INNER JOIN ENDERECO ENDER
    ON ENDER.ID_ENDERECO = AG.ID_ENDERECO
INNER JOIN CIDADE CITY
    ON CITY.ID_CIDADE = ENDER.ID_CIDADE
INNER JOIN ESTADO
    ON CITY.ID_ESTADO = ESTADO.ID_ESTADO
INNER JOIN PAIS
    ON PAIS.ID_PAIS = ESTADO.ID_PAIS;

SELECT *
FROM AGENCIA 
INNER JOIN CONTA 
    ON AGENCIA.CODIGO = CONTA.CODIGO_AGENCIA
INNER JOIN CLIENTE_X_CONTA CXC 
    ON CXC.CODIGO = CONTA.CODIGO
INNER JOIN CLIENTE 
    ON CLIENTE.CPF = CXC.ID_CLIENTE
INNER JOIN CLIENTE_X_ENDERECO CXE
    ON CLIENTE.CPF = CXE.ID_CLIENTE
INNER JOIN ENDERECO
    ON CXE.ID_ENDERECO = ENDERECO.ID_ENDERECO
INNER JOIN ENDERECO END_AGENCIA 
    ON AGENCIA.ID_ENDERECO = END_AGENCIA.ID_ENDERECO;

SELECT CLIENTE.*, CONTA.*, ENDERECO.*
FROM CLIENTE 
INNER JOIN CLIENTE_X_ENDERECO CXE ON CLIENTE.CPF = CXE.ID_CLIENTE
INNER JOIN ENDERECO ON CXE.ID_ENDERECO = ENDERECO.ID_ENDERECO
INNER JOIN CLIENTE_X_CONTA CXC ON CXE.ID_CLIENTE = CXC.ID_CLIENTE
INNER JOIN CONTA ON CXC.CODIGO = CONTA.CODIGO;




















