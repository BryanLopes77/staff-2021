package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/anao")
public class AnaoController {
    @Autowired
    private AnaoService service;

    @GetMapping("/")
    @ResponseBody
    public List<AnaoDTO> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public AnaoDTO findById(@PathVariable Integer id) {
        return this.service.findById(id);
    }

    @PostMapping("/save")
    @ResponseBody
    public AnaoDTO save(@RequestBody AnaoDTO anao) {
        return this.service.save(anao.convert());
    }

    @PutMapping("/{id}")
    @ResponseBody
    public AnaoDTO update(@RequestBody AnaoDTO anaoEntity, @PathVariable Integer id) {
        return this.service.update(anaoEntity.convert(), id);
    }
}
