package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.Inventario.ErroAoSalvarInventario;
import br.com.dbccompany.lotr.Exception.Inventario.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventario")
public class InventarioController {

    @Autowired
    private InventarioService service;

    @GetMapping("/")
    @ResponseBody
    public List<InventarioDTO> findAll() {
        return this.service.findAll();
    }

    @PostMapping("/all-by-personagem")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagem(@RequestBody List<PersonagemEntity> personagem) {
        return this.service.findAllByPersonagemIn(personagem);
    }

    @PostMapping("/all-by-inventarioItem")
    @ResponseBody
    public List<InventarioDTO> findAllByInventarioItem(@RequestBody List<Inventario_X_ItemId> inventarioXItemEntity) {
        return this.service.findAllByInventarioItemIn(inventarioXItemEntity);
    }

    @PostMapping("/all-by-personagens")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagemIn(@RequestBody List<PersonagemEntity> personagens) {
        return this.service.findAllByPersonagemIn(personagens);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public InventarioDTO findById(@PathVariable Integer id) throws InventarioNaoEncontrado {
        try{
            return this.service.findById(id);
        }catch ( Exception e ) {
            throw new InventarioNaoEncontrado();
        }
    }

    @PostMapping("/by-personagem")
    @ResponseBody
    public InventarioDTO findByPersonagem(@RequestBody PersonagemEntity personagem) {
        return this.service.findByPersonagem(personagem);
    }

    @PostMapping("/by-inventarioItem")
    @ResponseBody
    public InventarioDTO findByInventarioItem(@RequestBody Inventario_X_ItemId inventarioXItem) {
        return this.service.findByInventarioItem(inventarioXItem);
    }

    @PostMapping("/save")
    @ResponseBody
    public InventarioDTO save(@RequestBody InventarioDTO inventario) throws ErroAoSalvarInventario {
        try {
            return this.service.save(inventario.convert());
        }catch ( Exception e ) {
            throw new ErroAoSalvarInventario();
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public InventarioDTO update(@RequestBody InventarioEntity inventarioEntity, @PathVariable Integer id) {
        return this.service.update(inventarioEntity, id);
    }
}