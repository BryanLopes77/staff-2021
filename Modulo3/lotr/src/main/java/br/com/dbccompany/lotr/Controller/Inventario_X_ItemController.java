package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventarioItem")
public class Inventario_X_ItemController {
    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping("/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/all-by-quantidade/{quantidade}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByQuantidade(@PathVariable List<Integer> quantidade) {
        return this.service.findAllByQuantidade(quantidade);
    }

    @GetMapping("/all-by-inventario/{id}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByInventario(@PathVariable List<Integer> id) {
        return this.service.findAllByInventario(id);
    }

    @GetMapping("/all-by-item/{id}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByItem(@PathVariable List<ItemEntity> id) {
        return this.service.findAllByItem(id);
    }

    @PostMapping("/by-id")
    @ResponseBody
    public Inventario_X_ItemDTO findById(@RequestBody Inventario_X_ItemId id) {
        return this.service.findById(id);
    }

    @GetMapping("/by-quantidade/{quantidade}")
    @ResponseBody
    public Inventario_X_ItemDTO findByQuantidade(@PathVariable Integer quantidade) {
        return this.service.findByQuantidade(quantidade);
    }

    @GetMapping("/by-inventario/{id}")
    @ResponseBody
    public Inventario_X_ItemDTO findByInventario(@PathVariable Integer id) {
        return this.service.findByInventario(id);
    }

    @GetMapping("/by-item/{id}")
    @ResponseBody
    public Inventario_X_ItemDTO findByItem(@PathVariable ItemDTO id) {
        return this.service.findByItem(id.convert());
    }

    @PostMapping("/save")
    @ResponseBody
    public Inventario_X_ItemDTO save(@RequestBody Inventario_X_ItemDTO inventarioXItemEntity) {
        return this.service.save(inventarioXItemEntity.convert());
    }
}
