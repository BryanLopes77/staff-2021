package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Exception.Item.ErroAoSalvarItem;
import br.com.dbccompany.lotr.Exception.Item.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    private ItemService service;

    @GetMapping("/")
    @ResponseBody
    public List<ItemDTO> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ItemDTO findById(@PathVariable Integer id) throws ItemNaoEncontrado {
        try {
            return this.service.findById(id);
        }catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @PostMapping("/save")
    @ResponseBody
    public ItemDTO save(@RequestBody ItemDTO item) throws ErroAoSalvarItem {
        try {
            return this.service.save(item.convert());
        }catch ( Exception e )  {
            throw new ErroAoSalvarItem();
        }
    }
}

