package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public class AnaoDTO {
    private Integer id;
    private String nome;
    private Integer experiencia;
    private Double vida;
    private Double qtdDano;
    private Integer qtdExperienciaPorAtaque;
    private StatusEnum status;
    private InventarioDTO inventario;

    {
        this.vida = 100.0;
        this.inventario = new InventarioDTO();
        this.status = StatusEnum.RECEM_CRIADO;
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 10.0;
        this.experiencia = 0;
    }

    public AnaoDTO() {
    }

    public AnaoDTO(AnaoEntity elfo) {
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.experiencia = elfo.getExperiencia();
        this.vida = elfo.getVida();
        this.qtdDano = elfo.getQtdDano();
        this.qtdExperienciaPorAtaque = elfo.getQtdExperienciaPorAtaque();
        this.status = elfo.getStatus();
        this.inventario = elfo.getInventario() == null ? null : new InventarioDTO( elfo.getInventario() );
    }

    public AnaoEntity convert() {
        AnaoEntity anao = new AnaoEntity( this.nome );
        anao.setId(this.id);
        anao.setNome(this.nome);
        anao.setExperiencia(this.experiencia);
        anao.setVida(this.vida);
        anao.setQtdDano(this.qtdDano);
        anao.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        anao.setStatus(this.status);
        anao.setInventario(this.inventario.convert());
        return anao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQuantidadeDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario(InventarioDTO inventario) {
        this.inventario = inventario;
    }
}
