package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public class ElfoDTO {
    private Integer id;
    private String nome;
    private Integer experiencia;
    private Double vida;
    private Double qtdDano;
    private Integer qtdExperienciaPorAtaque;
    private StatusEnum status;
    private InventarioDTO inventario;

    {
        this.vida = 100.0;
        this.inventario = new InventarioDTO();
        this.status = StatusEnum.RECEM_CRIADO;
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 10.0;
        this.experiencia = 0;
    }

    public ElfoDTO() {
    }

    public ElfoDTO(ElfoEntity elfo) {
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.experiencia = elfo.getExperiencia();
        this.vida = elfo.getVida();
        this.qtdDano = elfo.getQtdDano();
        this.qtdExperienciaPorAtaque = elfo.getQtdExperienciaPorAtaque();
        this.status = elfo.getStatus();
        this.inventario = elfo.getInventario() == null ? null : new InventarioDTO( elfo.getInventario() );
    }

    public ElfoEntity convert() {
        ElfoEntity elfo = new ElfoEntity( this.nome );
        elfo.setId(this.id);
        elfo.setNome(this.nome);
        elfo.setExperiencia(this.experiencia);
        elfo.setVida(this.vida);
        elfo.setQtdDano(this.qtdDano);
        elfo.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        elfo.setStatus(this.status);
        elfo.setInventario(this.inventario.convert());
        return elfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario(InventarioDTO inventario) {
        this.inventario = inventario;
    }
}