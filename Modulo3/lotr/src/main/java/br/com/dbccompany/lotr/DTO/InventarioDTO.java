package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;

import java.util.ArrayList;
import java.util.List;

public class InventarioDTO {

    private Integer id;
    private List<Inventario_X_ItemDTO> inventarioItem;

    public InventarioDTO() {
    }

    public InventarioDTO(InventarioEntity inventario) {
        this.id = inventario.getId();

        ArrayList<Inventario_X_ItemDTO> inventarioItemDTO = new ArrayList<>();

        for (Inventario_X_Item inventarioXItemEntity : inventario.getInventarioItem()) {
            inventarioItemDTO.add(new Inventario_X_ItemDTO(inventarioXItemEntity));
        }

        this.inventarioItem = inventarioItemDTO;
    }

    public InventarioEntity convert() {
        InventarioEntity inventario = new InventarioEntity();
        inventario.setId(inventario.getId());
        ArrayList<Inventario_X_Item> inventarioXItem = new ArrayList<>();
        if (this.inventarioItem != null) {
            for (Inventario_X_ItemDTO inventarioXItemDTO : this.inventarioItem) {
                inventarioXItem.add(inventarioXItemDTO.convert());
            }
        }
        inventario.setInventarioItem(inventarioXItem);
        return inventario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Inventario_X_ItemDTO> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemDTO> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}