package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ItemEntity;

public class ItemDTO {
    private String descricao;
    private Integer id;

    public ItemDTO(){
    }

    public ItemDTO( ItemEntity item ) {
        this.descricao = item.getDescricao();
        this.id = item.getId();
    }

    public ItemEntity convert() {
        ItemEntity item = new ItemEntity( this.descricao );
        item.setId(this.id);
        item.setDescricao( this.descricao );
        return item;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
