package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class InventarioEntity {
    @Id
    @SequenceGenerator( name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ" )
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @OneToMany( mappedBy = "inventario", fetch = FetchType.LAZY )
    private List<Inventario_X_Item> inventarioItem;

    @OneToOne( mappedBy = "inventario" )
    private PersonagemEntity personagem;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Inventario_X_Item> getInventarioItem() {
        return this.inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_Item> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }
}
