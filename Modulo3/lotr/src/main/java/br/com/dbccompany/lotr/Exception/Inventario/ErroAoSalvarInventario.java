package br.com.dbccompany.lotr.Exception.Inventario;

import br.com.dbccompany.lotr.Exception.Item.ItemException;

public class ErroAoSalvarInventario extends ItemException {
    public ErroAoSalvarInventario() {
        super( "Erro ao salvar inventário, preencha todos os campos obrigatórios!" );
    }
}
