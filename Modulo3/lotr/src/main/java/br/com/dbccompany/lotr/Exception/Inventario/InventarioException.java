package br.com.dbccompany.lotr.Exception.Inventario;

public class InventarioException extends Exception {
    private String mensagem;

    public InventarioException( String mensagem ) {
        super( mensagem );
    }

    public String getMensagem() {
        return mensagem;
    }
}
