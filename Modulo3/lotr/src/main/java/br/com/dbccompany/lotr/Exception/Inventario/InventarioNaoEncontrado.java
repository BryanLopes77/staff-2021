package br.com.dbccompany.lotr.Exception.Inventario;

import br.com.dbccompany.lotr.Exception.Inventario.InventarioException;

public class InventarioNaoEncontrado extends InventarioException {
    public InventarioNaoEncontrado() {
        super( "Inventário não encontrado!" );
    }
}
