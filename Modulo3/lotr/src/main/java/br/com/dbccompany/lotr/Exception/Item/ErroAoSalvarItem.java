package br.com.dbccompany.lotr.Exception.Item;


public class ErroAoSalvarItem extends ItemException {
    public ErroAoSalvarItem() {
        super("Erro ao salvar, preencha todos os campos obrigatórios!");
    }
}
