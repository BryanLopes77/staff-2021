package br.com.dbccompany.lotr.Exception.Item;

public class ItemException extends Exception {
    private String mensagem;

    public ItemException( String mensagem ) {
        super( mensagem );
    }

    public String getMensagem() {
        return mensagem;
    }
}
