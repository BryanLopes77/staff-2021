package br.com.dbccompany.lotr.Exception.Item;


import br.com.dbccompany.lotr.Exception.Item.ItemException;

public class ItemNaoEncontrado extends ItemException {
    public ItemNaoEncontrado() {
        super("Item não encontrado!");
    }
}
