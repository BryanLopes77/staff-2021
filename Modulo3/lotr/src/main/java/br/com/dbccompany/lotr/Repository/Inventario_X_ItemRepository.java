package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_X_Item, Inventario_X_ItemId> {

    Optional<Inventario_X_Item> findById( Inventario_X_ItemId id );
    List<Inventario_X_Item> findAllByIdIn( List<Inventario_X_ItemId> ids );
    Inventario_X_Item findByInventario( Integer idInventario );
    List<Inventario_X_Item> findAllByInventarioIn( List<Integer> idInventario );
    Inventario_X_Item findByItem( ItemEntity idItem );
    List<Inventario_X_Item> findAllByItemIn( List<ItemEntity> idItens );
    Inventario_X_Item findByQuantidade( Integer qtd );
    List<Inventario_X_Item> findAllByQuantidadeIn( List<Integer> qtds );
}

