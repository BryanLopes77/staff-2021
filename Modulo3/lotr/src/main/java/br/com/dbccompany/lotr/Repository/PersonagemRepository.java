package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository <T extends PersonagemEntity> extends CrudRepository<T, Integer> {

    Optional<T> findById(Integer id );
    List<T> findAllByIdIn( List<Integer> ids );
    T findByNome( String nome );
    List<T> findAllByNomeIn( List<String> nome );
    T findByExperiencia( Integer experiencia );
    List<T> findAllByExperienciaIn( List<Integer> experiencias );
    T findByVida( Double vida );
    List<T> findAllByVidaIn( List<Double> vida );
    T findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtaque );
    List<T> findAllByQtdExperienciaPorAtaqueIn( List<Integer> qtdExperienciaPorAtaque );
    T findByStatus( StatusEnum status );
    List<T> findAllByStatusIn( List<StatusEnum> status );
    T findByInventario( InventarioEntity inventario );
}
