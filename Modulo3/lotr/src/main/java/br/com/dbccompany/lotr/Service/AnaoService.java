package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService {

    @Autowired
    private AnaoRepository repository;

    private AnaoEntity saveAndUpdate(AnaoEntity personagem ) {
        return repository.save( personagem );
    }

    public List<AnaoDTO> findAll() {
        List<AnaoEntity> anoes = (List<AnaoEntity>) repository.findAll();
        return this.convertEntityToDtoList( anoes );
    }

    @Transactional( rollbackFor = Exception.class )
    public AnaoDTO save( AnaoEntity personagem ) {
        AnaoEntity anao = this.repository.save( personagem );
        return new AnaoDTO( anao );
    }

    @Transactional( rollbackFor = Exception.class )
    public AnaoDTO update( AnaoEntity personagem, Integer id ) {
        personagem.setId( id );
        AnaoEntity anao = this.repository.save( personagem );
        return new AnaoDTO( anao );
    }

    public AnaoDTO findById( Integer id ) {
        AnaoEntity personagem = this.repository.findById( id ).orElse(null);
        return new AnaoDTO( personagem );
    }

    public List<AnaoDTO> findAllById( List<Integer> ids ) {
        List<AnaoEntity> anoes = this.repository.findAllByIdIn( ids );
        return this.convertEntityToDtoList( anoes );
    }

    public AnaoDTO findByNome( String nome ) {
        AnaoEntity anao = this.repository.findByNome( nome );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> findAllByNomeIn( List<String> nomes ) {
        List<AnaoEntity> anoes = this.repository.findAllByNomeIn(nomes);
        return this.convertEntityToDtoList( anoes );
    }

    public AnaoDTO findByExperiencia( Integer experiencia ) {
        AnaoEntity anao = this.repository.findByExperiencia( experiencia );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> findALlByExperienciaIn( List<Integer> experiencias ) {
        List<AnaoEntity> anoes = this.repository.findAllByExperienciaIn( experiencias );
        return this.convertEntityToDtoList( anoes );
    }

    public AnaoDTO findByVida( Double vida ) {
        AnaoEntity anao = this.repository.findByVida( vida );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> findAllByVida( List<Double> vida ) {
        List<AnaoEntity> anoes = this.repository.findAllByVidaIn( vida );
        return this.convertEntityToDtoList( anoes );
    }

    public AnaoDTO findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtq ) {
        AnaoEntity anao = this.repository.findByQtdExperienciaPorAtaque( qtdExperienciaPorAtq );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> findAllByQtdExperienciaPorAtaque( List<Integer> qtdExperienciaPorAtq ) {
        List<AnaoEntity> anoes = this.repository.findAllByQtdExperienciaPorAtaqueIn( qtdExperienciaPorAtq );
        return this.convertEntityToDtoList( anoes );
    }

    public AnaoDTO findByStatus( StatusEnum status ) {
        AnaoEntity anao = this.repository.findByStatus( status );
        return new AnaoDTO( anao );
    }

    public List<AnaoDTO> findAllByStatusIn( List<StatusEnum> status ) {
        List<AnaoEntity> anoes = this.repository.findAllByStatusIn( status );
        return this.convertEntityToDtoList( anoes );
    }

    public AnaoDTO findByInventario( InventarioEntity inventario ) {
        AnaoEntity anao = this.repository.findByInventario( inventario );
        return new AnaoDTO( anao );
    }

    private List<AnaoDTO> convertEntityToDtoList( List<AnaoEntity> anoes ) {
        ArrayList<AnaoDTO> anaoDto = new ArrayList<>();
        for( AnaoEntity anao : anoes ) {
            anaoDto.add( new AnaoDTO( anao ) );
        }
        return anaoDto;
    }
}

