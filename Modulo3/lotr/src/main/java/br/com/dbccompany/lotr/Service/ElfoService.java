package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService {

    @Autowired
    private ElfoRepository repository;

    private ElfoEntity saveAndUpdate(ElfoEntity personagem ) {
        return this.repository.save( personagem );
    }

    public List<ElfoDTO> findAll() {
        List<ElfoEntity> elfos = (List<ElfoEntity>) this.repository.findAll();
        return this.convertEntityToDtoList( elfos );
    }

    @Transactional( rollbackFor = Exception.class )
    public ElfoDTO save( ElfoEntity personagem ) {
        ElfoEntity anao = this.saveAndUpdate( personagem );
        return new ElfoDTO( anao );
    }

    @Transactional( rollbackFor = Exception.class )
    public ElfoDTO update( ElfoEntity personagem, Integer id ) {
        personagem.setId( id );
        ElfoEntity elfo = this.saveAndUpdate( personagem );
        return new ElfoDTO( elfo );
    }

    public ElfoDTO findById( Integer id ) {
        ElfoEntity personagem = this.repository.findById( id ).orElse(null);
        return new ElfoDTO( personagem );
    }

    public List<ElfoDTO> findAllById( List<Integer> ids ) {
        List<ElfoEntity> elfos = this.repository.findAllByIdIn( ids );
        return this.convertEntityToDtoList( elfos );
    }

    public ElfoDTO findByNome( String nome ) {
        ElfoEntity elfo = this.repository.findByNome( nome );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> findAllByNomeIn( List<String> nomes ) {
        List<ElfoEntity> elfos = this.repository.findAllByNomeIn( nomes );
        return this.convertEntityToDtoList( elfos );
    }

    public ElfoDTO findByExperiencia( Integer experiencia ) {
        ElfoEntity elfo = this.repository.findByExperiencia( experiencia );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> findALlByExperienciaIn( List<Integer> experiencias ) {
        List<ElfoEntity> elfos = this.repository.findAllByExperienciaIn(experiencias);
        return this.convertEntityToDtoList( elfos );
    }

    public ElfoDTO findByVida( Double vida ) {
        ElfoEntity elfo = this.repository.findByVida( vida );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> findAllByVida( List<Double> vida ) {
        List<ElfoEntity> elfos = this.repository.findAllByVidaIn( vida );
        return this.convertEntityToDtoList( elfos );
    }

    public ElfoDTO findByQtdExperienciaPorAtaque( Integer qtdExperienciaPorAtq ) {
        ElfoEntity elfo = this.repository.findByQtdExperienciaPorAtaque( qtdExperienciaPorAtq );
        return new ElfoDTO( elfo );
    }

    public List<ElfoDTO> findAllByQtdExperienciaPorAtaque( List<Integer> qtdExperienciaPorAtq ) {
        List<ElfoEntity> elfos = this.repository.findAllByQtdExperienciaPorAtaqueIn( qtdExperienciaPorAtq );
        return this.convertEntityToDtoList( elfos );
    }

    public ElfoDTO findByStatus( StatusEnum status ) {
        ElfoEntity elfos = this.repository.findByStatus( status );
        return new ElfoDTO( elfos );
    }

    public List<ElfoDTO> findAllByStatusIn( List<StatusEnum> status ) {
        List<ElfoEntity> anoes = this.repository.findAllByStatusIn( status );
        return this.convertEntityToDtoList( anoes );
    }

    public ElfoDTO findByInventario( InventarioEntity inventario ) {
        ElfoEntity elfo = this.repository.findByInventario( inventario );
        return new ElfoDTO( elfo );
    }

    private List<ElfoDTO> convertEntityToDtoList( List<ElfoEntity> elfos ) {
        ArrayList<ElfoDTO> elfoDto = new ArrayList<>();
        for( ElfoEntity elfo : elfos ) {
            elfoDto.add( new ElfoDTO( elfo ) );
        }
        return elfoDto;
    }
}