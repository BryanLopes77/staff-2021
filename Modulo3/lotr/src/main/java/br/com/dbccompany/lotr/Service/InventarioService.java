package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    public List<InventarioDTO> findAll() {
        List<InventarioEntity> inventarioEntities = (List<InventarioEntity>) this.repository.findAll();
        return this.convert( inventarioEntities );
    }

    public List<InventarioDTO> findAllByPersonagemIn( List<PersonagemEntity> personagens ) {
        List<InventarioEntity> inventario = this.repository.findAllByPersonagemIn( personagens );
        return this.convert( inventario );
    }

    public List<InventarioDTO> findAllByInventarioItemIn( List<Inventario_X_ItemId> invXItem ) {
        List<InventarioEntity> inventario = this.repository.findAllByInventarioItemIn( invXItem );
        return this.convert( inventario );
    }

    public InventarioDTO findById( Integer id ) {
        InventarioEntity inventarioEntity = this.repository.findById( id ).orElse(null);
        return new InventarioDTO( inventarioEntity );
    }

    public InventarioDTO findByPersonagem( PersonagemEntity personagem ) {
        InventarioEntity inventarioEntity = this.repository.findByPersonagem( personagem );
        return new InventarioDTO(inventarioEntity);
    }

    public InventarioDTO findByInventarioItem( Inventario_X_ItemId invXItem) {
        InventarioEntity inventarioEntity = this.repository.findByInventarioItem( invXItem );
        return new InventarioDTO( inventarioEntity );
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO save( InventarioEntity inventarioE ) {
        InventarioEntity inventario = this.saveAndUpdate( inventarioE );
        return new InventarioDTO(inventario);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO update(InventarioEntity inventarioEntity, Integer id) {
        inventarioEntity.setId(id);
        InventarioEntity inventario = this.saveAndUpdate(inventarioEntity);
        return new InventarioDTO(inventario);
    }

    private InventarioEntity saveAndUpdate(InventarioEntity inventarioEntity) {
        return this.repository.save(inventarioEntity);
    }

    private List<InventarioDTO> convert( List<InventarioEntity> inventario ) {
        ArrayList<InventarioDTO> inventarioDTO = new ArrayList<>();
        for ( InventarioEntity inventarioEntity : inventario ) {
            inventarioDTO.add( new InventarioDTO( inventarioEntity ) );
        }
        return inventarioDTO;
    }
}
