package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService {
    @Autowired
    private Inventario_X_ItemRepository repository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private InventarioRepository inventarioRepository;

    private Inventario_X_Item saveAndUpdate( Inventario_X_Item inventario_x_item ){
        Optional<ItemEntity> item = itemRepository.findById(inventario_x_item.getId().getId_item());
        Optional<InventarioEntity> inventario = inventarioRepository.findById(inventario_x_item.getId().getId_inventario());
        inventario_x_item.setInventario(inventario.get());
        inventario_x_item.setItem(item.get());
        return repository.save( inventario_x_item );
    }

    @Transactional(rollbackFor = Exception.class)
    public Inventario_X_ItemDTO save( Inventario_X_Item inventarioXItem ) {
        Inventario_X_Item inveXItem = this.saveAndUpdate( inventarioXItem );
        return new Inventario_X_ItemDTO( inveXItem );
    }

    @Transactional(rollbackFor = Exception.class)
    public Inventario_X_ItemDTO update( Inventario_X_Item inventarioXItem, Inventario_X_ItemId id) {
        inventarioXItem.setId(id);
        Inventario_X_Item invXItem = this.saveAndUpdate( inventarioXItem );
        return new Inventario_X_ItemDTO(invXItem);
    }

    public List<Inventario_X_ItemDTO> findAll() {
        List<Inventario_X_Item> inventarioXItem = ( List<Inventario_X_Item> ) this.repository.findAll();
        return this.convert( inventarioXItem );
    }

    public Inventario_X_ItemDTO findById( Inventario_X_ItemId id ) {
        Inventario_X_Item inventarioXItemEntity = this.repository.findById( id ).orElse(null);
        return new Inventario_X_ItemDTO( inventarioXItemEntity );
    }

    public List<Inventario_X_ItemDTO> findAllById( List<Inventario_X_ItemId> ids ) {
        List<Inventario_X_Item> invXIt = repository.findAllByIdIn( ids );
        return this.convert( invXIt );
    }

    public Inventario_X_ItemDTO findByInventario( Integer idInventario ) {
        Inventario_X_Item inventarioXItem = this.repository.findByInventario( idInventario );
        return new Inventario_X_ItemDTO( inventarioXItem );
    }

    public List<Inventario_X_ItemDTO> findAllByInventario( List<Integer> idInventario ) {
        List<Inventario_X_Item> inventarioXIt = this.repository.findAllByInventarioIn( idInventario );
        return this.convert( inventarioXIt );
    }

    public Inventario_X_ItemDTO findByItem( ItemEntity idItem ) {
        Inventario_X_Item inventarioXIt = this.repository.findByItem(idItem);
        return new Inventario_X_ItemDTO( inventarioXIt );
    }

    public List<Inventario_X_ItemDTO> findAllByItem(List<ItemEntity> idItem) {
        List<Inventario_X_Item> inventarioXIt = this.repository.findAllByItemIn( idItem );
        return this.convert( inventarioXIt );
    }

    public Inventario_X_ItemDTO findByQuantidade( Integer qtd ) {
        Inventario_X_Item inventarioXIt = this.repository.findByQuantidade( qtd );
        return new Inventario_X_ItemDTO( inventarioXIt );
    }

    public List<Inventario_X_ItemDTO> findAllByQuantidade( List<Integer> qtds ) {
        List<Inventario_X_Item> inventarioXIt = this.repository.findAllByQuantidadeIn( qtds );
        return this.convert( inventarioXIt );
    }

    private List<Inventario_X_ItemDTO> convert( List<Inventario_X_Item> inventarioXItemEntities ) {
        ArrayList<Inventario_X_ItemDTO> inventarioXItemDTOS = new ArrayList<>();

        for (Inventario_X_Item inventarioXItem : inventarioXItemEntities) {
            inventarioXItemDTOS.add(new Inventario_X_ItemDTO(inventarioXItem));
        }
        return inventarioXItemDTOS;
    }
}
