package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService {
    @Autowired
    private ItemRepository repository;

    public List<ItemDTO> findAll() {
        List<ItemEntity> itens = (List<ItemEntity>) this.repository.findAll();
        return this.converter(itens);
    }

    public ItemDTO findByDescricao( String descricao ) {
        ItemEntity item = this.repository.findByDescricao( descricao );
        return new ItemDTO( item );
    }

    public List<ItemDTO> findAllByDescricaoIn(List<String> descricoes) {
        List<ItemEntity> itens = this.repository.findAllByDescricaoIn(descricoes);
        return this.converter(itens);
    }

    public List<ItemDTO> findAllByInventarioItemIn( List<Inventario_X_ItemId> invXItem ) {
        List<ItemEntity> itens = this.repository.findAllByInventarioItemIn( invXItem );
        return this.converter( itens );
    }

    public ItemDTO findById(Integer id) {
        ItemEntity itemEntity = this.repository.findById(id).orElse(null);
        return new ItemDTO(itemEntity);
    }

    public ItemDTO findByInventarioItem( Inventario_X_ItemId invXItem ) {
        ItemEntity itemEntity = this.repository.findByInventarioItem( invXItem );
        return new ItemDTO( itemEntity );
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO save(ItemEntity itemEntity) {
        ItemEntity itemNovo = this.saveAndUpdate(itemEntity);
        return new ItemDTO(itemNovo);
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO update(ItemEntity itemEntity, int id) {
        itemEntity.setId(id);
        ItemEntity itemAtualizado = this.saveAndUpdate(itemEntity);
        return new ItemDTO(itemAtualizado);
    }

    private ItemEntity saveAndUpdate(ItemEntity itemEntity) {
        return this.repository.save(itemEntity);
    }

    private List<ItemDTO> converter( List<ItemEntity> item ) {
        ArrayList<ItemDTO> itemDTOS = new ArrayList<>();

        for ( ItemEntity itemEntity : item ) {
            itemDTOS.add( new ItemDTO( itemEntity ) );
        }

        return itemDTOS;
    }
}
