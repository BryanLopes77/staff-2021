function calcularCirculo({raio, tipoCalculo}) {
    switch(tipoCalculo) {
        case "A":
            return Math.PI * Math.pow(raio, 2);
        
        case "C":
            return 2 * Math.PI * raio;
    }
}

const valor = {
    raio: 2,
    tipoCalculo: "A"
}

const valor1 = {
    raio: 10,
    tipoCalculo: "C"
}

console.log(calcularCirculo(valor));
console.log(calcularCirculo(valor1));

function bissexto(ano) {
    return ( ano %400 == 0 ) || ( ano %4 == 0 && ano %100 != 0 );
}

console.log(bissexto(2016));
console.log(bissexto(2017));

function somarPares(numeros) {
    let somar = 0;
    for(let i = 0; i < numeros.length; i+=2) {
        somar += numeros[i];
    }
    return somar;
}

console.log(somarPares([2,4,6,7,-2]));

let adicionar = value1 => value2 => value1 + value2;

console.log(adicionar(3)(4));
console.log(adicionar(5)(2));

function imprimirBRL(numero) {
    numero = Math.ceil(numero*100)/100;
    return numero.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
}

console.log(imprimirBRL(2313477.01353));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(0));


