class Jogador {
    constructor( nome, numeroDaCamiseta ) {
        this._nome = nome;
        this._numeroDaCamiseta = numeroDaCamiseta;
    }

    get nome() {
        return this._nome;
    }

    get numeroDaCamiseta() {
        return this._numeroDaCamiseta;
    }
}

class Time {
    constructor( nome, tipoEsporte, ligaQueJoga ) {
        this._nome = nome;
        this._esporte = tipoEsporte;
        this._ligaQueJoga = ligaQueJoga;
        this._status = "Ativo";
        this._jogadores = [];
    }

    adicionarJogador( Jogador ) {
        this._jogadores.push( Jogador );
    }

    get jogadores() {
        return this._jogadores;
    }

    buscarJogadoresPorNome = ( nomeJogador ) => {
        return this._jogadores.filter( jogador => jogador.nome == nomeJogador );
    }

    buscarJogadoresPorNumero = ( numeroCamisetaJogador ) => { 
        return this._jogadores.filter( jogador => jogador.numeroDaCamiseta == numeroCamisetaJogador );
    }
}

let bryan = new Jogador("Bryan", 9);
let gabriel = new Jogador("Gabriel", 10);
let gremio = new Time("Grêmio", "Futebol", "Brasileirão");

gremio.adicionarJogador(bryan);
gremio.adicionarJogador(gabriel);

console.log(gremio.jogadores);
console.log( gremio.buscarJogadoresPorNome("Bryan") );
console.log( gremio.buscarJogadoresPorNumero(10) );


