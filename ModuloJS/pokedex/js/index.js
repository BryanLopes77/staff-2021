
const pokeApi = new PokeApi();
let idAtual = 0;

const renderizar = ( pokemon ) => {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome;

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const pokeId = dadosPokemon.querySelector( '.pokeId' );
  pokeId.innerHTML = `#${ pokemon.pokeId }`;
  idAtual = pokemon.pokeId;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  const tipos = dadosPokemon.querySelector( '.tipo' );
  tipos.innerHTML = 'Tipo(s):'
  const ul = document.createElement( 'ul' );
  ul.classList.add( 'relative' );
  tipos.appendChild( ul );
  console.log( pokemon );

  pokemon.tipo.map( tipo => {
    const li = document.createElement( 'li' );
    li.innerHTML = `${ tipo.type.name } `;
    li.classList.add( 'relative' );
    return ul.appendChild( li );
  } );

  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  estatistica.innerHTML = 'Estatísticas:';
  const ulStat = document.createElement( 'ul' );
  ul.classList.add( 'relative' );
  estatistica.appendChild( ulStat );

  pokemon.estatistica.map( stats => {
    const li = document.createElement( 'li' );
    li.innerHTML = ` ${ stats.stat.name } : ${ stats.base_stat } `;
    li.classList.add( 'relative' );
    return ulStat.appendChild( li );
  } );
}

const buscarPorId = () => { // eslint-disable-line no-unused-vars
  const entradaId = document.getElementById( 'entradaId' ).value;
  if ( idAtual === entradaId ) {
    return;
  }
  let erroId = document.getElementById( 'erroId' );
  pokeApi
    .buscarEspecifico( entradaId )
    .then( pokemon => {
      const poke = new Pokemon( pokemon )
      console.log( poke );
      renderizar( poke );
    }, erro => {
      erroId = document.getElementById( 'erroId' );
      erroId.innerHTML = 'ID INVÁLIDO';
      console.log( erro );
    } );
}

const pokemonAleatorio = () => { // eslint-disable-line no-unused-vars
  const gerarNumero = ( inicio, fim ) => Math.floor( Math.random() * ( fim - inicio + 1 ) ) + inicio
  const armazenarNumeros = window.localStorage.getItem( gerarNumero( 1, 897 ) );

  if ( armazenarNumeros === gerarNumero( 1, 897 ) ) {
    return;
  }

  let erroId = document.getElementById( 'erroId' );
  const entradaId = document.getElementById( 'entradaId' );
  entradaId.value = gerarNumero( 1, 897 );
  window.localStorage.setItem( gerarNumero( 1, 897 ), gerarNumero( 1, 897 ) );

  pokeApi
    .buscarEspecifico( gerarNumero( 1, 897 ) )
    .then( pokemon => {
      const poke = new Pokemon( pokemon )
      renderizar( poke );
    }, erro => {
      erroId = document.getElementById( 'erroId' );
      erroId.innerHTML = 'ID já sorteado anteriormente, tente novamente!'
      console.log( erro );
    } );
}
