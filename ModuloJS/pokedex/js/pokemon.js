class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._pokeId = objDaApi.id;
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;
    this._tipo = objDaApi.types;
    this._estatistica = objDaApi.stats;
  }

  get pokeId() {
    return this._pokeId;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 } cm`
  }

  get peso() {
    return `${ this._peso / 10 } kg`
  }

  get tipo() {
    return this._tipo;
  }

  get estatistica() {
    return this._estatistica;
  }
}
