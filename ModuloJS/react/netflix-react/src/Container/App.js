import React, { Component } from 'react';

import './App.css';
import ListaSeries from './models/ListaSeries';

export default class App extends Component {
  constructor( props ) {
    super( props );
    const series = new ListaSeries();
    
    console.log( series.invalidas() );

    console.log( series.filtrarPorAno( 2000 ) );

    console.log( series.procurarPorNome( "Amanda de Carli" ) );
    console.log( series.procurarPorNome( "Bryan Lopes" ) );
    console.log( series.procurarPorNome( "Alex Baptista" ) );
    console.log( series.procurarPorNome( "Teste" ) );

    console.log( series.mediaDeEpisodios() );

    console.log( series.totalSalarios( 0 ) );
    console.log( series.totalSalarios( 1 ) );

    console.log( series.queroGenero( "Suspense" ) );

    console.log( series.queroTitulo( "The" ) );

    console.log( series.hashtasg() );


  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          
        </header>
      </div>
    );
  }
}  