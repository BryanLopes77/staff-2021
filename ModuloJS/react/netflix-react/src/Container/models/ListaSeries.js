import Serie from "./Serie";
import PropTypes from "prop-types";

export default class ListaSeries {
    constructor() {
      this.todos = [
        {
          "titulo": "Stranger Things",
          "anoEstreia": 2016,
          "diretor": [
            "Matt Duffer",
            "Ross Duffer"
          ],
          "genero": [
            "Suspense",
            "Ficcao Cientifica",
            "Drama"
          ],
          "elenco": [
            "Winona Ryder",
            "David Harbour",
            "Finn Wolfhard",
            "Millie Bobby Brown",
            "Gaten Matarazzo",
            "Caleb McLaughlin",
            "Natalia Dyer",
            "Charlie Heaton",
            "Cara Buono",
            "Matthew Modine",
            "Noah Schnapp"
          ],
          "temporadas": 2,
          "numeroEpisodios": 17,
          "distribuidora": "Netflix"
        },
        {
          "titulo": "Game Of Thrones",
          "anoEstreia": 2011,
          "diretor": [
            "David Benioff",
            "D. B. Weiss",
            "Carolyn Strauss",
            "Frank Doelger",
            "Bernadette Caulfield",
            "George R. R. Martin"
          ],
          "genero": [
            "Fantasia",
            "Drama"
          ],
          "elenco": [
            "Peter Dinklage",
            "Nikolaj Coster-Waldau",
            "Lena Headey",
            "Emilia Clarke",
            "Kit Harington",
            "Aidan Gillen",
            "Iain Glen ",
            "Sophie Turner",
            "Maisie Williams",
            "Alfie Allen",
            "Isaac Hempstead Wright"
          ],
          "temporadas": 7,
          "numeroEpisodios": 67,
          "distribuidora": "HBO"
        },
        {
          "titulo": "The Walking Dead",
          "anoEstreia": 2010,
          "diretor": [
            "Jolly Dale",
            "Caleb Womble",
            "Paul Gadd",
            "Heather Bellson"
          ],
          "genero": [
            "Terror",
            "Suspense",
            "Apocalipse Zumbi"
          ],
          "elenco": [
            "Andrew Lincoln",
            "Jon Bernthal",
            "Sarah Wayne Callies",
            "Laurie Holden",
            "Jeffrey DeMunn",
            "Steven Yeun",
            "Chandler Riggs ",
            "Norman Reedus",
            "Lauren Cohan",
            "Danai Gurira",
            "Michael Rooker ",
            "David Morrissey"
          ],
          "temporadas": 9,
          "numeroEpisodios": 122,
          "distribuidora": "AMC"
        },
        {
          "titulo": "Band of Brothers",
          "anoEstreia": 20001,
          "diretor": [
            "Steven Spielberg",
            "Tom Hanks",
            "Preston Smith",
            "Erik Jendresen",
            "Stephen E. Ambrose"
          ],
          "genero": [
            "Guerra"
          ],
          "elenco": [
            "Damian Lewis",
            "Donnie Wahlberg",
            "Ron Livingston",
            "Matthew Settle",
            "Neal McDonough"
          ],
          "temporadas": 1,
          "numeroEpisodios": 10,
          "distribuidora": "HBO"
        },
        {
          "titulo": "The JS Mirror",
          "anoEstreia": 2017,
          "diretor": [
            "Lisandro",
            "Jaime",
            "Edgar"
          ],
          "genero": [
            "Terror",
            "Caos",
            "JavaScript"
          ],
          "elenco": [
            "Amanda de Carli",
            "Alex Baptista",
            "Gilberto Junior",
            "Gustavo Gallarreta",
            "Henrique Klein",
            "Isaias Fernandes",
            "João Vitor da Silva Silveira",
            "Arthur Mattos",
            "Mario Pereira",
            "Matheus Scheffer",
            "Tiago Almeida",
            "Tiago Falcon Lopes"
          ],
          "temporadas": 5,
          "numeroEpisodios": 40,
          "distribuidora": "DBC"
        },
        {
          "titulo": "Mr. Robot",
          "anoEstreia": 2018,
          "diretor": [
            "Sam Esmail"
          ],
          "genero": [
            "Drama",
            "Techno Thriller",
            "Psychological Thriller"
          ],
          "elenco": [
            "Rami Malek",
            "Carly Chaikin",
            "Portia Doubleday",
            "Martin Wallström",
            "Christian Slater"
          ],
          "temporadas": 3,
          "numeroEpisodios": 32,
          "distribuidora": "USA Network"
        },
        {
          "titulo": "Narcos",
          "anoEstreia": 2015,
          "diretor": [
            "Paul Eckstein",
            "Mariano Carranco",
            "Tim King",
            "Lorenzo O Brien"
          ],
          "genero": [
            "Documentario",
            "Crime",
            "Drama"
          ],
          "elenco": [
            "Wagner Moura",
            "Boyd Holbrook",
            "Pedro Pascal",
            "Joann Christie",
            "Mauricie Compte",
            "André Mattos",
            "Roberto Urbina",
            "Diego Cataño",
            "Jorge A. Jiménez",
            "Paulina Gaitán",
            "Paulina Garcia"
          ],
          "temporadas": 3,
          "numeroEpisodios": 30,
          "distribuidora": null
        },
        {
          "titulo": "Westworld",
          "anoEstreia": 2016,
          "diretor": [
            "Athena Wickham"
          ],
          "genero": [
            "Ficcao Cientifica",
            "Drama",
            "Thriller",
            "Acao",
            "Aventura",
            "Faroeste"
          ],
          "elenco": [
            "Anthony I. Hopkins",
            "Thandie N. Newton",
            "Jeffrey S. Wright",
            "James T. Marsden",
            "Ben I. Barnes",
            "Ingrid N. Bolso Berdal",
            "Clifton T. Collins Jr.",
            "Luke O. Hemsworth"
          ],
          "temporadas": 2,
          "numeroEpisodios": 20,
          "distribuidora": "HBO"
        },
        {
          "titulo": "Breaking Bad",
          "anoEstreia": 2008,
          "diretor": [
            "Vince Gilligan",
            "Michelle MacLaren",
            "Adam Bernstein",
            "Colin Bucksey",
            "Michael Slovis",
            "Peter Gould"
          ],
          "genero": [
            "Acao",
            "Suspense",
            "Drama",
            "Crime",
            "Humor Negro"
          ],
          "elenco": [
            "Bryan Cranston",
            "Anna Gunn",
            "Aaron Paul",
            "Dean Norris",
            "Betsy Brandt",
            "RJ Mitte"
          ],
          "temporadas": 5,
          "numeroEpisodios": 62,
          "distribuidora": "AMC"
        }
      ].map( series => new Serie( series ) );
    }

    invalidas() {
        let seriesInvalidas = this.todos.filter( serie => serie.anoEstreia > new Date().getFullYear() 
                              || Object.values( serie ).some( vazio => vazio === null || typeof vazio === "undefined" ));
        return `Séries Inválidas: ${ seriesInvalidas.map( serieInvalida => serieInvalida.titulo ).join(" - ") }`
    };

    filtrarPorAno( ano ) {
      let seriesPorAno = this.todos.filter( serie => serie.anoEstreia >= ano );
      return `Séries a partir de ${ ano }: ${ seriesPorAno.map( seriePorAno => seriePorAno.titulo ).join(" - ") } `
    }

    procurarPorNome( nome ) {
      let buscarPorNome = this.todos.filter( nomeBuscado => nomeBuscado.elenco.find( elencoInteiro => elencoInteiro === nome ) );
      return buscarPorNome.length >= 1 ? true : false;
    }

    mediaDeEpisodios() {
      let buscarEps = this.todos.map( ep => ep.numeroEpisodios ).reduce(( total, valor ) => total + valor, 0);
      return parseInt( buscarEps / this.todos.length );
    }

    totalSalarios( indiceSerie ) {
      let diretores = 100000 * this.todos[indiceSerie].diretor.length;
      let operarios = 40000 * this.todos[indiceSerie].elenco.length;
      let salarioTotal = (diretores + operarios).toLocaleString( "pt-br", { style: "currency", currency: "BRL" } ).replace( ".", "." );
      return `Salário do elenco: ${ salarioTotal }`;
    }

    queroGenero( gnr ) {
      let buscaPorGenero = this.todos.filter( generoBuscado => generoBuscado.genero.find( generos => generos === gnr ) );
      return `Resultado para "${ gnr }": ${ buscaPorGenero.map( series => series.titulo ).join( " - " ) }`;
    }
    
    queroTitulo( ttl ) {
      let titulosBuscados = this.todos.filter( serie => ( serie.titulo.includes( ttl ) ) )
      return `Titulo(s): ${ titulosBuscados.map( series => series.titulo ) }`;
    }

    hashtasg() {
      let elencoAbrev = this.todos.filter( abreviados => abreviados.elenco.every( todoElenco => todoElenco.match( '\\s[A-Z].\\s' ) ) );
      let tag = elencoAbrev[0].elenco.map( nome => nome.split( ' ' ).find( sobrenome => sobrenome.includes( '.' ) ) ).join('').replaceAll( '.', '' );
      return `#${ tag }`;
    }
}

ListaSeries.propTypes = {
    invalidas: PropTypes.array,
    filtrarPorAno: PropTypes.array,
    procurarPorNome: PropTypes.array,
    mediaDeEpisodios: PropTypes.array,
    totalSalarios: PropTypes.array,
    queroGenero: PropTypes.array,
    queroTitulo: PropTypes.array,
    hashtasg: PropTypes.array
}