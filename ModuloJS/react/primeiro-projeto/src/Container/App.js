import React, { Component } from 'react';

import ListaEpisodios from './models/ListaEpisodios';
import EpisodioUi from '../Components/EpisodioUi';
import MensagemFlash from '../Components/MensagemFlash';
import MeuInputNumero from '../Components/MeuInputNumero';

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.sortear = this.sortear.bind( this );
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      deveExibirErro: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;

    this.setState((state) => {
      return { 
            ...state,
            episodio
      }
    });
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.assistido();
    this.setState((state) => {
      return { 
            ...state,
            episodio
      }
    });
  }

  registrarNota( { erro, nota } ) {
    this.setState((state) => {
      return { 
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }
    
    const { episodio } = this.state;
    let mensagem, corMensagem;
    
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota );
      corMensagem = 'verde';
      mensagem = 'Nota registrada com sucesso!';
    }else {
      corMensagem = 'vermelho';
      mensagem = 'Informar uma nota válida (entre 1 e 5)!';
    }

    this.exibirMensagem({ corMensagem, mensagem });
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState((state) => {
      return { 
            ...state,
            deveExibirMensagem: true,
            mensagem,
            corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return { 
            ...state,
            deveExibirMensagem: devoExibir
      }
    });
  }
  
  render() {
    const { episodio, deveExibirMensagem, corMensagem, mensagem, deveExibirErro } = this.state;

    return (
      <div className="App">
        <MensagemFlash atualizar={ this.atualizarMensagem } exibir={ deveExibirMensagem } mensagem={ mensagem } cor={ corMensagem } />
        <header className="App-header">
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear } >Próximo</button>
            <button className="btn azul" onClick={ this.marcarComoAssistido } >Já Assisti!</button>
          </div>
          <span>Nota: { episodio.nota }</span>
          <MeuInputNumero
              placeholder="Nota de 1 a 5"
              mensagem="Qual a sua nota para esse episódio?"
              visivel={ episodio.foiAssistido || false }
              obrigatorio
              exibirErro={ deveExibirErro }
              atualizarValor={ this.registrarNota.bind( this ) }/>
        </header>
      </div>
    );
  }
}