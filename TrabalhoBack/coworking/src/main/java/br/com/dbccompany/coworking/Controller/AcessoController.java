package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/acesso" )
@CrossOrigin(origins = "*")
public class AcessoController {

    @Autowired
    private AcessoService service;

    @PostMapping(value = "/acessar")
    @ResponseBody
    public ResponseEntity<AcessoDTO> registrarAcesso(@RequestBody AcessoDTO acessoDTO) {
        try {
            return new ResponseEntity<>( service.acesso(acessoDTO), HttpStatus.ACCEPTED  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public HttpEntity<? extends Object> salvarEspacoPacote(@RequestBody AcessoDTO acessoDTO) {
        try {
            return new ResponseEntity<>( service.salvarEntrada(acessoDTO.converter()), HttpStatus.ACCEPTED  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public HttpEntity<? extends Object> buscarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.trazerPorId(id), HttpStatus.ACCEPTED );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public HttpEntity<? extends Object> buscarTodos() {
        try {
            return new ResponseEntity<>(service.trazerTodos(), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
