package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping( value = "/api/cliente")
@CrossOrigin(origins = "*")
public class ClienteController {


    @Autowired
    private ClienteService service;

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public HttpEntity<? extends Object> buscarClientePorId(@PathVariable Integer id ){
        try {
            ClienteDTO cliente = service.buscarPorId(id);
            ClienteEntity clienteDTO = new ClienteDTO( cliente.converter() ).converter();
            return new ResponseEntity<Object>( clienteDTO, HttpStatus.ACCEPTED);
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public HttpEntity<? extends Object> buscarTodosClientes(){
        try {
            return new ResponseEntity<Object>(service.buscarTodos(), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<ClienteDTO> salvar(@RequestBody ClienteDTO clientes ){
        try {
            return new ResponseEntity<>( service.salvarComContato(clientes), HttpStatus.ACCEPTED  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.NOT_FOUND );
        }
    }

    @PutMapping ( value = "/editar/{id}")
    @ResponseBody
    public HttpEntity<? extends Object> editarClientes(@RequestBody ClienteDTO clientes, @PathVariable Integer id){
        try {
            return new ResponseEntity<Object>( service.editar(clientes, id), HttpStatus.ACCEPTED );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/buscar/cpf/{cpf}")
    @ResponseBody
    public ResponseEntity<Object> buscarPorCpf(@PathVariable String cpf) {
        try {
            return new ResponseEntity<Object>(Optional.ofNullable(service.buscarPorCpf(cpf)).get(), HttpStatus.ACCEPTED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
