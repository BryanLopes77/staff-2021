package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/clientePacote")
@CrossOrigin(origins = "*")
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;


    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientePacoteDTO> todosClientePacote() throws PreencherCorretamenteOsCampos {
        List<ClientePacoteDTO> listaDTO = new ArrayList<>();
        for (ClientePacoteDTO cliente : service.trazerTodosOsClientePacote()) {
            listaDTO.add(new ClientePacoteDTO(cliente.converter()));
        }
        return listaDTO;
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ClientePacoteDTO salvar(@RequestBody ClientePacoteDTO clientePacoteDTO) throws PreencherCorretamenteOsCampos {
        ClientePacoteDTO dd = service.salvar(clientePacoteDTO.converter());
        return new ClientePacoteDTO( dd.converter() );
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ClientePacoteDTO clientePacoteEspecifico(@PathVariable Integer id) throws PreencherCorretamenteOsCampos {
        ClientePacoteEntity clientePacoteEntity = service.buscarPorId(id).converter();
        return new ClientePacoteDTO( clientePacoteEntity );
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientePacoteDTO editarPacoteCliente(@PathVariable Integer id, @RequestBody ClientePacoteDTO clientePacoteDTO) throws PreencherCorretamenteOsCampos {
        ClientePacoteEntity clientePacote = clientePacoteDTO.converter();
        clientePacote.setId(id);
        ClientePacoteDTO newDTO = new ClientePacoteDTO(service.salvar(clientePacote).converter());
        return newDTO;
    }
}