package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(value = "/api/contato")
@CrossOrigin(origins = "*")
public class ContatoController {

    @Autowired
    private ContatoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContatoDTO> salvar(@RequestBody ContatoDTO contato) {
        try {

            return new ResponseEntity<>(service.salvar(contato), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public HttpEntity<? extends Object> buscarTodos(){
        try{
            return new ResponseEntity<Object>(service.findAll(), HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editar(@RequestBody ContatoDTO contatoDTO, @PathVariable Integer id) {
        try {
            ContatoEntity contatoEntity = contatoDTO.converter();
            contatoEntity.setId(id);
            return new ResponseEntity<>(new ContatoDTO(service.salvarEEditar(contatoEntity)), HttpStatus.ACCEPTED);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}