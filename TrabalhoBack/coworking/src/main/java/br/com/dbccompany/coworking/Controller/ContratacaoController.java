package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/contratacao")
@CrossOrigin(origins = "*")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> salvar (@RequestBody ContratacaoDTO contratacao){
        try{
            return new ResponseEntity<>(service.salvarContratacao(contratacao.converter()), HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> editar (@RequestBody ContratacaoDTO contratacao, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editarContratacao(contratacao.converter(), id), HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarAll(), HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.trazerPorId(id), HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
