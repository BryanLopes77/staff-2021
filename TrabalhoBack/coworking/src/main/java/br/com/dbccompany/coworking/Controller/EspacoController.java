package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espaco")
@CrossOrigin(origins = "*")
public class EspacoController {

    @Autowired
    private EspacoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<EspacoDTO> salvar(@RequestBody EspacoDTO espaco){
        try{
            return new ResponseEntity<>(service.salvar(espaco.converter()), HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<EspacoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public HttpEntity<? extends Object> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
