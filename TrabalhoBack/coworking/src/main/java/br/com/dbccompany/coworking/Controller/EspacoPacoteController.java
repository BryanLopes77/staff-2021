package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espacoPacote")
@CrossOrigin(origins = "*")
public class EspacoPacoteController {

    @Autowired
    private EspacoPacoteService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<EspacoPacoteDTO> buscarPorId(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<EspacoPacoteDTO>> buscarTodos() {
        try {
            return new ResponseEntity<>(service.trazerTodosEspacoPacote(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<EspacoPacoteDTO> salvarEspacoPacote(@RequestBody EspacoPacoteDTO espacoPacoteDTO) {
        try {
            return new ResponseEntity<>( service.salvar(espacoPacoteDTO), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<EspacoPacoteDTO> editar(@RequestBody EspacoPacoteDTO pacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.editar(pacote, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}