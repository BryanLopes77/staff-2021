package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pacote")
@CrossOrigin(origins = "*")
public class PacoteController {

    @Autowired
    PacoteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PacoteDTO> todosPacote() throws PreencherCorretamenteOsCampos {
        logger.info("Buscando pacotes.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<PacoteDTO> listaDTO = new ArrayList<>();
        for (PacoteDTO pacote : service.buscarTodos()) {
            listaDTO.add(new PacoteDTO(pacote.converter()));
        }
        return listaDTO;
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public PacoteDTO salvar(@RequestBody PacoteDTO pacoteDTO) throws PreencherCorretamenteOsCampos {
        logger.info("Adicionando novo pacote.");
        PacoteEntity pacoteEntity = pacoteDTO.converter();
        PacoteDTO newDto = new PacoteDTO(service.salvar(pacoteEntity).converter());
        return newDto;
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public PacoteDTO pacoteEspecifico(@PathVariable Integer id) throws PreencherCorretamenteOsCampos {
        logger.info("Buscando pacote de id: " + id);
        PacoteDTO pacoteDTO = new PacoteDTO(service.buscarPorId(id).converter());
        return pacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PacoteDTO editarPacote(@PathVariable Integer id, @RequestBody PacoteDTO pacoteDTO) throws PreencherCorretamenteOsCampos {
        logger.info("Editando pacote de id: " + id);
        PacoteEntity pacote = pacoteDTO.converter();
        PacoteDTO newDTO = new PacoteDTO(service.editar(pacote, id).converter());
        return newDTO;
    }
}