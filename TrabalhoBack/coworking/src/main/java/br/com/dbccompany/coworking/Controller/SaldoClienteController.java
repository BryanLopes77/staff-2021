package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/saldoCliente")
@CrossOrigin(origins = "*")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoClienteDTO> buscarTodos() throws PreencherCorretamenteOsCampos {
        try {
            List<SaldoClienteDTO> listaDTO = new ArrayList<>();
            for (SaldoClienteEntity saldoCliente : service.trazerTodos()) {
                listaDTO.add(new SaldoClienteDTO(saldoCliente));
            }
            return listaDTO;
        } catch (Exception e) {
            return (List<SaldoClienteDTO>) new ResponseEntity<Object>( null, HttpStatus.NOT_FOUND );
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<SaldoClienteDTO> salvar(@RequestBody SaldoClienteDTO saldoCliente) {
        try {
            return new ResponseEntity<>( service.salvar( saldoCliente ), HttpStatus.ACCEPTED  );
        }catch( Exception e ){
            e.getMessage();
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @GetMapping(value = "/ver/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoClienteDTO saldoClienteEspecifico(@PathVariable Integer idCliente, @PathVariable Integer idEspaco) throws PreencherCorretamenteOsCampos {
        Optional<SaldoClienteEntity> optionalEntity = service.buscarPorId(idCliente, idEspaco);
        SaldoClienteEntity saldoClienteEntity = optionalEntity.isPresent() ? optionalEntity.get() : null;
        SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO(saldoClienteEntity);
        return saldoClienteDTO;
    }

    @PutMapping(value = "/editar/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoClienteDTO editarSaldoCliente(@PathVariable Integer idCliente, @PathVariable Integer idEspaco, @RequestBody SaldoClienteDTO saldoClienteDTO) throws PreencherCorretamenteOsCampos {
        SaldoClienteEntity saldoCliente = saldoClienteDTO.converter();
        SaldoClienteDTO newDTO = new SaldoClienteDTO(service.editar(saldoCliente, idCliente, idEspaco));
        return newDTO;
    }
}