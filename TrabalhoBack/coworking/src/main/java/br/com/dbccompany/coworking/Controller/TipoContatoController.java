package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
@CrossOrigin(origins = "*")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoDTO> todosTipoContato() throws PreencherCorretamenteOsCampos {
        List<TipoContatoDTO> listaDTO = new ArrayList<>();
        for (TipoContatoDTO tipoContato : service.trazerTodosOsItens()) {
            listaDTO.add(new TipoContatoDTO(tipoContato.converter()));
        }
        return listaDTO;
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public TipoContatoDTO salvar(@RequestBody TipoContatoDTO tipoContatoDTO) throws PreencherCorretamenteOsCampos {
        TipoContatoEntity tipoContatoEntity = tipoContatoDTO.converter();
        TipoContatoDTO newDto = new TipoContatoDTO(service.salvarEEditar(tipoContatoEntity).converter());
        return newDto;
    }

    @GetMapping(value = "/buscar/{id}")
    @ResponseBody
    public TipoContatoDTO buscarPorId(@PathVariable Integer id) throws PreencherCorretamenteOsCampos {
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.buscarPorId(id).converter());
        return tipoContatoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContatoDTO editarTipoContato(@PathVariable Integer id, @RequestBody TipoContatoDTO tipoContatoDTO) {
        TipoContatoEntity tipoContato = tipoContatoDTO.converter();
        tipoContato.setId(id);
        return new TipoContatoDTO( service.salvarEEditar(tipoContato).converter() );
    }
}