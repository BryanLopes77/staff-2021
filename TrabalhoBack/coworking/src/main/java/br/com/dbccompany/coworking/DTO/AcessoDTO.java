package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDateTime;

public class AcessoDTO {
    private Integer id;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;
    private Boolean isEntrada;
    private Boolean isExcecao;
    private LocalDateTime data;
    private String mensagem;
    private ClienteDTO cliente;
    private EspacoDTO espaco;

    public AcessoDTO() {}

    public AcessoDTO(AcessoEntity acesso) {
        this.id = acesso.getId();
        this.saldoCliente = acesso.getSaldoCliente();
        this.isEntrada = acesso.getEntrada();
        this.data = acesso.getData();
        this.isExcecao = acesso.getExcecao();
        this.cliente = new ClienteDTO( acesso.getSaldoCliente().getCliente() );
        this.espaco = new EspacoDTO( acesso.getSaldoCliente().getEspaco() );
    }

    public AcessoEntity converter() {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.saldoCliente);
        acesso.setEntrada(this.isEntrada);
        acesso.setData( this.data == null ? LocalDateTime.now() : this.data );
        acesso.setExcecao(this.isExcecao);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
