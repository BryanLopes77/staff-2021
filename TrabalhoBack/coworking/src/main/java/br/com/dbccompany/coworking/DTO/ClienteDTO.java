package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;

import java.util.*;

public class ClienteDTO {
    private Integer id;
    private String nome;
    private String cpf;
    private Date dataDeNascimento;
    private List<ContatoDTO> contatos;
    private List<SaldoClienteEntity> saldosClientes;
    private List<ClientePacoteEntity> clientePacote;
    private List<ContratacaoEntity> contratacao;

    public ClienteDTO() {}

    public ClienteDTO(ClienteEntity cliente) {
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = cliente.getCpf().length() == 11 ? cliente.getCpf() : null;
        this.dataDeNascimento = cliente.getDataDeNascimento();
        this.contatos = this.convertEntityToDTOList( cliente.getContatos() );
        this.saldosClientes = cliente.getSaldosClientes();
        this.clientePacote = cliente.getClientePacoteEntities();
        this.contratacao = cliente.getContratacao();
    }

    public ClienteEntity converter() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(this.id);
        cliente.setNome(this.nome);
        cliente.setCpf(this.getCpf().length() == 11 ? this.cpf : null);
        cliente.setDataDeNascimento(this.dataDeNascimento);
        cliente.setContatos(this.convertDTOToEntityList(this.contatos));
        cliente.setSaldosClientes(this.saldosClientes);
        cliente.setClientePacoteEntities(this.clientePacote);
        cliente.setContratacao(this.contratacao);
        return cliente;
    }

    private ArrayList<ContatoDTO> convertEntityToDTOList(List<ContatoEntity> entidades ){
        ArrayList<ContatoDTO> contatos = new ArrayList<>();
        for( ContatoEntity entidade:entidades){
            contatos.add( new ContatoDTO( entidade ) );
        }
        return contatos;
    }

    private List<ContatoEntity> convertDTOToEntityList(List<ContatoDTO> dtos){
        ArrayList<ContatoEntity> contatosConvertidos = new ArrayList<>();
        for( ContatoDTO dto : dtos){
            contatosConvertidos.add( dto.converter() );
        }
        return contatosConvertidos;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(Date dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public List<ContatoDTO> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoDTO> contatos) {
        this.contatos = contatos;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<ClientePacoteEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
