package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;

import java.util.List;

public class ClientePacoteDTO {

    private Integer id;
    private ClienteDTO cliente;
    private PacoteDTO pacote;
    private Integer quantidade;
    private List<PagamentoEntity> pagamentos;

    public ClientePacoteDTO() {}

    public ClientePacoteDTO(ClientePacoteEntity clientePacote) {
        this.id = clientePacote.getId();
        this.cliente = new ClienteDTO(clientePacote.getCliente());
        this.pacote = new PacoteDTO(clientePacote.getPacote());
        this.quantidade = clientePacote.getQuantidade();
        this.pagamentos = clientePacote.getPagamentos();
    }

    public ClientePacoteEntity converter() {
        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setId(this.id);
        clientePacote.setCliente(this.getCliente() != null ? this.cliente.converter() : null);
        clientePacote.setPacote(this.getPacote() != null ? this.pacote.converter() : null);
        clientePacote.setQuantidade(this.quantidade);
        clientePacote.setPagamentos(this.pagamentos);
        return clientePacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}