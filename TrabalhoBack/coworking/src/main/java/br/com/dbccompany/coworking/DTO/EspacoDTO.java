package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Util.ConverterDinheiro;

import java.util.ArrayList;
import java.util.List;

public class EspacoDTO {

    private Integer id;
    private String nome;
    private Integer qntPessoas;
    private String valor;

    public EspacoDTO(EspacoEntity espaco){
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qntPessoas = espaco.getQtdPessoas();
        this.valor = ConverterDinheiro.converterParaSaida(espaco.getValor());
    }

    public EspacoDTO(){

    }

    public EspacoEntity converter(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qntPessoas);
        espaco.setValor(this.getValor() != null ? ConverterDinheiro.converterParaEntrada(this.valor) : null);
        return espaco;
    }


    private List<ContratacaoDTO> converterContratacaoEntityParaDTO(List<ContratacaoEntity> contratacoes){
        List<ContratacaoDTO> contrsDTO = new ArrayList<>();
        for(ContratacaoEntity contr : contratacoes){
            contrsDTO.add(new ContratacaoDTO(contr));
        }
        return contrsDTO;
    }

    private List<ContratacaoEntity> converterContratacaoDTOParaEntity(List<ContratacaoDTO> contratacoes){
        List<ContratacaoEntity> listaEnt = new ArrayList<>();
        for(ContratacaoDTO contr : contratacoes){
            listaEnt.add(contr.converter());
        }
        return listaEnt;
    }

    private List<EspacoPacoteDTO> converterEspacoPacotesEntityParaDTO(List<EspacoPacoteEntity> espacoPacotes){
        List<EspacoPacoteDTO> espacoPacoteDTOS = new ArrayList<>();
        for(EspacoPacoteEntity espPct : espacoPacotes){
            espacoPacoteDTOS.add(new EspacoPacoteDTO(espPct));
        }
        return espacoPacoteDTOS;
    }

    private List<EspacoPacoteEntity> converterEspacoPacotesDTOParaEntity(List<EspacoPacoteDTO> espacoPacotes){
        List<EspacoPacoteEntity> listEnt = new ArrayList<>();
        for(EspacoPacoteDTO espPct : espacoPacotes){
            listEnt.add(espPct.converter());
        }
        return listEnt;
    }

    private List<SaldoClienteDTO> converterSaldoClienteEntityParaDTO(List<SaldoClienteEntity> saldoClientes){
        List<SaldoClienteDTO> listaDTO = new ArrayList<>();
        for(SaldoClienteEntity elemento : saldoClientes){
            listaDTO.add(new SaldoClienteDTO(elemento));
        }
        return listaDTO;
    }

    private List<SaldoClienteEntity> converterSaldoClienteDTOParaEntity(List<SaldoClienteDTO> saldoClientes){
        List<SaldoClienteEntity> listEnt = new ArrayList<>();
        for(SaldoClienteDTO saldoClienteDTO : saldoClientes){
            listEnt.add(saldoClienteDTO.converter());
        }
        return listEnt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQntPessoas() {
        return qntPessoas;
    }

    public void setQntPessoas(Integer qntPessoas) {
        this.qntPessoas = qntPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }


}