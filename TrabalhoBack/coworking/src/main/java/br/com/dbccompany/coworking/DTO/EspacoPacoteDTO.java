package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.Enum.*;

public class EspacoPacoteDTO {

    private Integer id;
    private TipoDeContratacaoEnum tipoDeContratacaoEnum;
    private Integer quantidade;
    private Integer prazo;
    private EspacoEntity espaco;
    private PacoteEntity pacote;

    public EspacoPacoteDTO(EspacoPacoteEntity espacoPacote) {
        this.id = espacoPacote.getId();
        this.tipoDeContratacaoEnum = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
        this.espaco = espacoPacote.getEspaco();
        this.pacote = espacoPacote.getPacote();
    }

    public EspacoPacoteDTO() {}

    public EspacoPacoteEntity converter() {
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setTipoContratacao(this.tipoDeContratacaoEnum);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        espacoPacote.setEspaco(this.espaco);
        espacoPacote.setPacote(this.pacote);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoDeContratacaoEnum getTipoContratacao() {
        return tipoDeContratacaoEnum;
    }

    public void setTipoContratacao(TipoDeContratacaoEnum tipoDeContratacaoEnum) {
        this.tipoDeContratacaoEnum = tipoDeContratacaoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }
}