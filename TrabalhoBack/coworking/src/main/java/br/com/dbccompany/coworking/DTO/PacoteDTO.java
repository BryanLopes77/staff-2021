package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Util.ConverterDinheiro;

import java.util.List;

public class PacoteDTO {

    private Integer id;
    private List<EspacoPacoteEntity> espacosPacotes;
    private String valor;
    private ClientePacoteDTO clientesPacotes;

    public PacoteDTO() {}

    public PacoteDTO(PacoteEntity pacote) {
        this.id = pacote.getId();
        this.espacosPacotes = pacote.getEspacoPacoteEntities();
        this.valor = ConverterDinheiro.converterParaSaida(pacote.getValor());
        if( clientesPacotes != null ) {
            this.clientesPacotes =  new ClientePacoteDTO((ClientePacoteEntity) pacote.getClientePacoteEntities());
        }
    }

    public PacoteEntity converter() {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setId(this.id);
        pacote.setEspacoPacoteEntities(this.espacosPacotes);
        pacote.setValor(ConverterDinheiro.converterParaEntrada(this.valor));
        if( clientesPacotes != null ) {
            pacote.setClientePacoteEntities((List<ClientePacoteEntity>) this.clientesPacotes.converter());
        }
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClientePacoteDTO getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientePacoteDTO clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
