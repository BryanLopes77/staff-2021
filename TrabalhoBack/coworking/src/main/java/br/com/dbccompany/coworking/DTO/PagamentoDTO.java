package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Enum.TipoDePagamentoEnum;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;

import java.util.List;

public class PagamentoDTO {

    private Integer id;
    private ClientePacoteDTO clientePacote;
    private ContratacaoDTO contratacao;
    private TipoDePagamentoEnum tipoPagamento;
    private List<SaldoClienteDTO> saldosGerado;

    public PagamentoDTO(PagamentoEntity pagamento){
        this.id = pagamento.getId();
        this.clientePacote = pagamento.getClientePacote() != null ? new ClientePacoteDTO(pagamento.getClientePacote()) : null;
        this.contratacao = pagamento.getContratacao() != null ? new ContratacaoDTO(pagamento.getContratacao()) : null;
        this.tipoPagamento = pagamento.getTipoDePagamento();
    }
    public PagamentoDTO(){

    }

    public PagamentoEntity converter(){
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setId(this.id);
        pagamento.setClientePacote(this.clientePacote != null ? this.clientePacote.converter() : null);
        pagamento.setContratacao(this.contratacao != null ? this.contratacao.converter() : null);
        pagamento.setTipoDePagamento(this.tipoPagamento);
        return pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public TipoDePagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoDePagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public List<SaldoClienteDTO> getSaldosGerado() {
        return saldosGerado;
    }

    public void setSaldosGerado(List<SaldoClienteDTO> saldosGerado) {
        this.saldosGerado = saldosGerado;
    }
}
