package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.List;

public class TipoContatoDTO {

    private Integer id;
    private String tipoDeContato;
    private List<ContatoEntity> contatos;

    public TipoContatoDTO() {}

    public TipoContatoDTO(TipoContatoEntity tipoContato) {
        this.id = tipoContato.getId();
        this.tipoDeContato = tipoContato.getTipoDeContato();
        this.contatos = tipoContato.getContatos();
    }

    public TipoContatoEntity converter() {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setTipoDeContato(this.tipoDeContato);
        tipoContato.setContatos(this.contatos);
        return tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoDeContato() {
        return tipoDeContato;
    }

    public void setTipoDeContato(String tipoDeContato) {
        this.tipoDeContato = tipoDeContato;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}