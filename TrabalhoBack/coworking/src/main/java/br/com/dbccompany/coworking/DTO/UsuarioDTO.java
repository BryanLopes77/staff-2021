package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;

public class UsuarioDTO {
    private String nome;
    private String email;
    private String login;
    private String senha;

    public UsuarioDTO( UsuarioEntity usuarioEntity ) {
        this.nome = usuarioEntity.getNome();
        this.email = usuarioEntity.getEmail();
        this.login = usuarioEntity.getLogin();
        this.senha = usuarioEntity.getSenha();
    }

    public UsuarioDTO( ) {

    }

    public UsuarioEntity converter() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome( this.nome );
        usuario.setEmail( this.email );
        usuario.setLogin( this.login );
        usuario.setSenha( this.senha );
        return usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}

