package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ" )
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumns({
            @JoinColumn( name = "id_cliente", nullable = false ),
            @JoinColumn( name = "id_espaco", nullable = false )
    })
    private SaldoClienteEntity saldoCliente;

    private Boolean isEntrada;

    private LocalDateTime data;

    private Boolean isExcecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada( Boolean entrada ) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
