package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class ClienteEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ" )
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true, length = 11 )
    private String cpf;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    @Column( nullable = false, length = 10 )
    private Date dataDeNascimento;

    @OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<ContatoEntity> contatos;

    @OneToMany( mappedBy = "cliente", fetch = FetchType.LAZY )
    private List<ClientePacoteEntity> clientePacoteEntities;

    @OneToMany( mappedBy = "cliente", fetch = FetchType.LAZY )
    private List<SaldoClienteEntity> saldosClientes;

    @OneToMany( mappedBy = "cliente", fetch = FetchType.LAZY )
    private List<ContratacaoEntity> contratacao;

    public ClienteEntity() {
        List<ContatoEntity> lst = new ArrayList<>();
        lst.add(new ContatoEntity());
        lst.add(new ContatoEntity());
        contatos = lst;
    }

    public ClienteEntity(String nome, String cpf, Date dataNascimento) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataDeNascimento = dataNascimento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(Date dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ClientePacoteEntity> getClientePacoteEntities() {
        return clientePacoteEntities;
    }

    public void setClientePacoteEntities(List<ClientePacoteEntity> clientePacoteEntities) {
        this.clientePacoteEntities = clientePacoteEntities;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
