package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ClientePacoteEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( name = "CLIENTEPACOTE_SEQ", sequenceName = "CLIENTEPACOTE_SEQ" )
    @GeneratedValue( generator = "CLIENTEPACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private Integer quantidade;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_cliente" )
    private ClienteEntity cliente;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_pacote" )
    private PacoteEntity pacote;

    @OneToMany( mappedBy = "clientePacote" )
    private List<PagamentoEntity> pagamentos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
