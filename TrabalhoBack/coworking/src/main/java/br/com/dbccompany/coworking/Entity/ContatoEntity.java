package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class ContatoEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( sequenceName = "CONTATO_SEQ", name = "CONTATO_SEQ" )
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_tipoDeContato" )
    private TipoContatoEntity tipoContato;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_cliente" )
    @JsonIgnore
    private ClienteEntity cliente;

    @Column( nullable = false )
    private String valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
