package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( sequenceName = "CONTRATACAO_SEQ", name = "CONTRATACAO_SEQ" )
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "id_espaco" )
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn( name = "id_cliente" )
    private ClienteEntity cliente;

    @Enumerated( EnumType.STRING )
    @Column( nullable = false )
    private TipoDeContratacaoEnum tipoDeContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    private Double desconto;

    private Integer prazo;

    @OneToMany( mappedBy = "contratacao" )
    private List<PagamentoEntity> pagamentos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public TipoDeContratacaoEnum getTipoDeContratacao() {
        return tipoDeContratacao;
    }

    public void setTipoDeContratacao(TipoDeContratacaoEnum tipoDeContratacao) {
        this.tipoDeContratacao = tipoDeContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
