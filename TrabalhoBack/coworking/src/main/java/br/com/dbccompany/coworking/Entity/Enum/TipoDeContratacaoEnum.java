package br.com.dbccompany.coworking.Entity.Enum;


public enum TipoDeContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
