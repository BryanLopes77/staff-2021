package br.com.dbccompany.coworking.Entity.Enum;

public enum TipoDePagamentoEnum {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
