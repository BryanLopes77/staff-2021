package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( sequenceName = "ESPACO_SEQ", name = "ESPACO_SEQ" )
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false, unique = true )
    private String nome;

    @Column( nullable = false )
    private Integer qtdPessoas;

    @Column( nullable = false )
    private Double valor;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "espaco" )
    private List<EspacoPacoteEntity> espacoPacoteEntities;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "espaco" )
    private List<SaldoClienteEntity> saldosClientes;

    @OneToMany( fetch = FetchType.LAZY, mappedBy = "espaco" )
    private List<ContratacaoEntity> contratacao;

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacoPacoteEntities() {
        return espacoPacoteEntities;
    }

    public void setEspacoPacoteEntities(List<EspacoPacoteEntity> espacoPacoteEntities) {
        this.espacoPacoteEntities = espacoPacoteEntities;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }
}
