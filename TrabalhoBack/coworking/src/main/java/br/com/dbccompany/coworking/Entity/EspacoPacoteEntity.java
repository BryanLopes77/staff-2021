package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;

import javax.persistence.*;

@Entity
public class EspacoPacoteEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( sequenceName = "ESPACOPACOTE_SEQ", name = "ESPACOPACOTE_SEQ" )
    @GeneratedValue( generator = "ESPACOPACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Enumerated( EnumType.STRING )
    private TipoDeContratacaoEnum tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false )
    private Integer prazo;

    @ManyToOne( cascade = CascadeType.ALL )
    private PacoteEntity pacote;

    @ManyToOne( cascade = CascadeType.ALL )
    private EspacoEntity espaco;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoDeContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoDeContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }
}
