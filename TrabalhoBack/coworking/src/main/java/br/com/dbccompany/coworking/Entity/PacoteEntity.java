package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( sequenceName = "PACOTE_SEQ", name = "PACOTE_SEQ" )
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    private Double valor;

    @OneToMany( mappedBy = "pacote" )
    private List<EspacoPacoteEntity> espacoPacoteEntities;

    @OneToMany( mappedBy = "pacote" )
    private List<ClientePacoteEntity> clientePacoteEntities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacoPacoteEntities() {
        return espacoPacoteEntities;
    }

    public void setEspacoPacoteEntities(List<EspacoPacoteEntity> espacoPacoteEntities) {
        this.espacoPacoteEntities = espacoPacoteEntities;
    }

    public List<ClientePacoteEntity> getClientePacoteEntities() {
        return clientePacoteEntities;
    }

    public void setClientePacoteEntities(List<ClientePacoteEntity> clientePacoteEntities) {
        this.clientePacoteEntities = clientePacoteEntities;
    }
}
