package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Entity.Enum.TipoDePagamentoEnum;

import javax.persistence.*;

@Entity
public class PagamentoEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated( EnumType.STRING )
    private TipoDePagamentoEnum tipoDePagamento;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_clientePacote" )
    private ClientePacoteEntity clientePacote;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_contratacao" )
    private ContratacaoEntity contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoDePagamentoEnum getTipoDePagamento() {
        return tipoDePagamento;
    }

    public void setTipoDePagamento(TipoDePagamentoEnum tipoDePagamento) {
        this.tipoDePagamento = tipoDePagamento;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }
}
