package br.com.dbccompany.coworking.Entity;


import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoClienteEntity extends AbstractEntity<SaldoClienteId> {
    @EmbeddedId
    private SaldoClienteId id = new SaldoClienteId();

    @ManyToOne
    @MapsId( "id_cliente" )
    private ClienteEntity cliente;

    @ManyToOne
    @MapsId( "id_espaco" )
    private EspacoEntity espaco;

    @Enumerated( EnumType.STRING )
    @Column( nullable = false )
    private TipoDeContratacaoEnum tipoDeContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false, length = 10 )
    private LocalDate vencimento;

    @OneToMany( mappedBy = "saldoCliente" )
    private List<AcessoEntity> acessos;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoDeContratacaoEnum getTipoDeContratacao() {
        return tipoDeContratacao;
    }

    public void setTipoDeContratacao(TipoDeContratacaoEnum tipoDeContratacao) {
        this.tipoDeContratacao = tipoDeContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public List<AcessoEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessoEntity> acessos) {
        this.acessos = acessos;
    }
}
