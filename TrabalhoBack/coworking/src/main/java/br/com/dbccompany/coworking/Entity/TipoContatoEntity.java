package br.com.dbccompany.coworking.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContatoEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( sequenceName = "TIPOCONTATO_SEQ", name = "TIPOCONTATO_SEQ" )
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private String tipoDeContato;

    @JsonIgnore
    @OneToMany( mappedBy = "tipoContato", fetch = FetchType.LAZY )
    private List<ContatoEntity> contatos;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoDeContato() {
        return tipoDeContato;
    }

    public void setTipoDeContato(String tipoDeContato) {
        this.tipoDeContato = tipoDeContato;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
