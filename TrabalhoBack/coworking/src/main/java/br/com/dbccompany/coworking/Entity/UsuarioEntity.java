package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;

@Entity
public class UsuarioEntity extends AbstractEntity<Integer> {
    @Id
    @SequenceGenerator( name = "USUARIO_SEQ", sequenceName = "USURARIO_SEQ" )
    @GeneratedValue( generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE )
    protected Integer id;

    @Column( unique = true, nullable = false )
    private String email;

    @Column( unique = true, nullable = false )
    private String login;

    @Column( nullable = false )
    private String senha;

    @Column( nullable = false )
    private String nome;

    public UsuarioEntity( String email, String login, String senha, String nome ) {
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.nome = nome;
    }

    public UsuarioEntity() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
