package br.com.dbccompany.coworking.Exception;

public class ObjetoInexistente extends Exception {

    private String mensagem;

    public ObjetoInexistente(String mensagem){
        super(mensagem);
    }

    public String getMensagem() {
        return mensagem;
    }
}
