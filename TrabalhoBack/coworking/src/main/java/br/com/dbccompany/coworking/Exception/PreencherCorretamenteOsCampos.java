package br.com.dbccompany.coworking.Exception;

public class PreencherCorretamenteOsCampos extends ObjetoInexistente {
    public PreencherCorretamenteOsCampos() {
        super("Preencher corretamente todos os campos obrigatórios!");
    }
}
