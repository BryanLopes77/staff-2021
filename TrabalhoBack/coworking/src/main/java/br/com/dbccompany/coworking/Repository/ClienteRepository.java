package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    Optional<ClienteEntity> findById(Integer id );
    List<ClienteEntity> findAllByIdIn( List<Integer> ids );
    ClienteEntity findByNome( String nome );
    List<ClienteEntity> findAllByNomeIn( List<String> nomes );
    ClienteEntity findByCpf(String cpf );
    List<ClienteEntity> findAllByCpf( String cpfs );
    ClienteEntity findByDataDeNascimento(Date data);
}
