package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    Optional<ContatoEntity> findById( Integer id );
    List<ContatoEntity> findAllByIdIn( List<Integer> ids );
    List<ContatoEntity> findByTipoContato( TipoContatoEntity tipoContato );
    List<ContatoEntity> findAllByTipoContatoIn( List<TipoContatoEntity> tipoContatos );
    ContatoEntity findByValor( String valor );
    List<ContatoEntity> findAllByValorIn( List<String> valores );
}
