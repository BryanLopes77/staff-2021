package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    Optional<ContratacaoEntity> findById(Integer id );
    List<ContratacaoEntity> findAllById( Integer id );
    List<ContratacaoEntity> findAllByEspaco( EspacoEntity espaco );
    List<ContratacaoEntity> findAllByCliente( ClienteEntity cliente );
    List<ContratacaoEntity> findAllByTipoDeContratacao( TipoDeContratacaoEnum tipoDeContratacaoEnum );
    List<ContratacaoEntity> findAllByQuantidade( Integer quantidade );
    List<ContratacaoEntity> findAllByDesconto( Double desconto );
    List<ContratacaoEntity> findAllByPrazo( Integer prazo );
    List<ContratacaoEntity> findAll();
}
