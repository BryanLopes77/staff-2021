package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    Optional<EspacoPacoteEntity> findById(Integer id );
    List<EspacoPacoteEntity> findAllById( Integer id );
    List<EspacoPacoteEntity> findByTipoContratacao( TipoDeContratacaoEnum tipoDeContratacaoEnum );
    List<EspacoPacoteEntity> findByQuantidade( Integer quantidade );
    List<EspacoPacoteEntity> findByPrazo( Integer prazo );
    List<EspacoPacoteEntity> findAllByPacote( PacoteEntity pacote );
    List<EspacoPacoteEntity> findByEspaco( EspacoEntity espacoEntity );
}