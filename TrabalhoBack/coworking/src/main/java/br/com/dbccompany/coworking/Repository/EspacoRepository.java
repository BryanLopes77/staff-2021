package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    Optional<EspacoEntity> findById( Integer id );
    List<EspacoEntity> findAllByIdIn( List<Integer> ids );
    EspacoEntity findByNome( String nome );
    List<EspacoEntity> findAllByNomeIn( List<String> nomes );
    List<EspacoEntity> findByQtdPessoas( Integer qtdPessoas );
}
