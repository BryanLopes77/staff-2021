package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoDePagamentoEnum;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    Optional<PagamentoEntity> findById( Integer id );
    List<PagamentoEntity> findAll();
    List<PagamentoEntity> findAllById( Integer ids );
    List<PagamentoEntity> findAllByClientePacote( ClientePacoteEntity clientePacote );
    List<PagamentoEntity> findAllByContratacao( ContratacaoEntity contratacao );
    PagamentoEntity findByTipoDePagamento( TipoDePagamentoEnum tipoDePagamentoEnum );
    List<PagamentoEntity> findAllByTipoDePagamento( TipoDePagamentoEnum tipoDePagamentoEnum );

}
