package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    Optional<SaldoClienteEntity> findById( SaldoClienteId id );
    Optional<SaldoClienteEntity> findByCliente( ClienteEntity cliente );
    List<SaldoClienteEntity> findAllByCliente( ClienteEntity cliente );
    Optional<SaldoClienteEntity> findByTipoDeContratacao( TipoDeContratacaoEnum tipoContratacao );
    List<SaldoClienteEntity> findAllByTipoDeContratacao( TipoDeContratacaoEnum tipoContratacao );
    Optional<SaldoClienteEntity> findByVencimento( Date vencimento );
    List<SaldoClienteEntity> findAllByVencimento( Date vencimento );
    SaldoClienteEntity findByQuantidade( Integer qtd );
}
