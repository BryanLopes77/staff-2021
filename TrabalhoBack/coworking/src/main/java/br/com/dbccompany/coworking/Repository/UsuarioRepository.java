package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    UserDetails findByLogin(String login );
    List<UsuarioEntity> findAllByLoginIn( List<String> logins );
    Optional<UsuarioEntity> findById( Integer id );
    List<UsuarioEntity> findAllByIdIn( List<Integer> ids );
    UsuarioEntity findByNome( String nome );
    List<UsuarioEntity> findAllByNomeIn( List<String> nomes );
    UsuarioEntity findByEmail( String email );
    List<UsuarioEntity> findAllByEmailIn( List<String> emails );
}
