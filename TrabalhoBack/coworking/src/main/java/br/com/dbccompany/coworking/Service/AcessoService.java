package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.ConverterTempo;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService {
    @Autowired
    AcessoRepository repository;
    @Autowired
    SaldoClienteRepository saldoRepository;
    @Autowired
    SaldoClienteService saldoClienteService;
    @Autowired
    ClienteRepository clienteRepository;
    @Autowired
    EspacoRepository espacoRepository;

    public AcessoDTO acesso(AcessoDTO acessoDTO) throws PreencherCorretamenteOsCampos {
        try {
            AcessoEntity acesso = acessoDTO.converter();
            SaldoClienteId saldoClienteId = new SaldoClienteId();
            saldoClienteId.setId_cliente(acessoDTO.getCliente().getId());
            saldoClienteId.setId_espaco(acessoDTO.getEspaco().getId());
            acesso.setSaldoCliente(saldoRepository.findById(saldoClienteId).get());
            if (acesso.getEntrada()) {
                return salvarEntrada(acesso);
            } else {
                return salvarSaida(acesso);
            }
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public AcessoDTO salvarEntrada(AcessoEntity acesso) throws PreencherCorretamenteOsCampos {
        try {
            Integer saldo = acesso.getSaldoCliente().getQuantidade();
            AcessoDTO acessoDTO = new AcessoDTO();
            if (acesso.getSaldoCliente().getVencimento().isBefore(LocalDate.now()) || saldo <= 0) {
                SaldoClienteEntity saldoFinal = acesso.getSaldoCliente();
                saldoFinal.setQuantidade(0);
                saldoRepository.save(saldoFinal);
            } else if (acesso.getData() == null) {
                acesso.setData(LocalDateTime.now());
            } else if (acesso.getData().isAfter(LocalDateTime.now())) {
                repository.save(acesso);
                acessoDTO.setMensagem("Saldo disponivel: " + saldo + acesso.getSaldoCliente().getTipoDeContratacao().toString());
            }
            return acessoDTO;
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public AcessoDTO salvarSaida ( AcessoEntity acesso ) throws PreencherCorretamenteOsCampos {
        try {
            List<AcessoEntity> acessos = repository.findAllBySaldoCliente(acesso.getSaldoCliente());
            AcessoEntity ultimoAcesso = acessos.get(acessos.size() - 1);
            Integer tempoGasto = Math.toIntExact(Duration.between(ultimoAcesso.getData(), acesso.getData()).toMinutes());
            TipoDeContratacaoEnum tipoContratacao = acesso.getSaldoCliente().getTipoDeContratacao();
            Integer saldoAnterior = acesso.getSaldoCliente().getQuantidade();
            Integer saldoFinal = saldoAnterior - ConverterTempo.converterTempo(tempoGasto, tipoContratacao);
            acesso.getSaldoCliente().setQuantidade(saldoFinal);
            repository.save(acesso);

            AcessoDTO acessoDTO = new AcessoDTO();
            acessoDTO.setMensagem("Saldo: " + saldoFinal + acesso.getSaldoCliente().getTipoDeContratacao().toString());
            return acessoDTO;
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public AcessoDTO trazerPorId(Integer id) throws PreencherCorretamenteOsCampos {
        try {
            Optional<AcessoEntity> contato = repository.findById(id);
            return new AcessoDTO(repository.findById(id).get());
        }  catch( Exception e ){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public List<AcessoDTO> trazerTodos() throws PreencherCorretamenteOsCampos {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private List<AcessoDTO> convertList( Iterable<AcessoEntity> entidades ){
        ArrayList<AcessoDTO> convertidos = new ArrayList<AcessoDTO>();
        for( AcessoEntity entidade:entidades){
            convertidos.add( new AcessoDTO( entidade ) );
        }
        return convertidos;
    }
}

