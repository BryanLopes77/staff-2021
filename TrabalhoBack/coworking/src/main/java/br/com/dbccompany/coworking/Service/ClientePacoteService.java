package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientePacoteDTO;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientePacoteService{
    @Autowired
    private ClientePacoteRepository repository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    public List<ClientePacoteDTO> trazerTodosOsClientePacote() throws PreencherCorretamenteOsCampos {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ClientePacoteDTO salvar( ClientePacoteEntity clientePacoteDTO ) throws PreencherCorretamenteOsCampos {
        try {
            ClientePacoteEntity clientePacote = clientePacoteDTO;
            clientePacote.setCliente( clienteRepository.findById( clientePacoteDTO.getCliente().getId() ).get() );
            clientePacote.setPacote( pacoteRepository.findById( clientePacoteDTO.getPacote().getId() ).get() );
            return this.salvarEEditar( clientePacote );
        } catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ClientePacoteDTO editar(ClientePacoteDTO cliente, Integer id ) throws PreencherCorretamenteOsCampos {
        try{
            ClientePacoteEntity clienteEntity = cliente.converter();
            clienteEntity.setId(id);
            return this.salvarEEditar( clienteEntity );
        }catch(Exception e){
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private ClientePacoteDTO salvarEEditar(ClientePacoteEntity entity ){
        ClientePacoteEntity elementNovo = repository.save( entity );
        return new ClientePacoteDTO( elementNovo );
    }

    public ClientePacoteDTO buscarPorId(Integer id) throws PreencherCorretamenteOsCampos {
        try{
            Optional<ClientePacoteEntity> element = repository.findById(id);
            return new ClientePacoteDTO( repository.findById(id).get() );
        }catch( Exception e ){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private List<ClientePacoteDTO> convertList(Iterable<ClientePacoteEntity> entidades ){
        ArrayList<ClientePacoteDTO> conversao = new ArrayList<>();
        for( ClientePacoteEntity entidade:entidades){
            conversao.add( new ClientePacoteDTO( entidade ) );
        }
        return conversao;
    }

}