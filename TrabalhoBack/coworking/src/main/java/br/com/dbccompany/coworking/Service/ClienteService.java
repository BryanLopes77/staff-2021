package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.*;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ContatoRepository repository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;


    public ClienteEntity salvarEEditar(ClienteEntity cliente) {
        return clienteRepository.save(cliente);
    }

    public ClienteDTO editar( ClienteDTO clienteEntity, Integer id ) {
        ClienteEntity clienteEntity1 = clienteEntity.converter();
        clienteEntity1.setId(id);
        return new ClienteDTO( clienteRepository.save(clienteEntity1) );
    }

    public ClienteDTO salvarComContato( ClienteDTO cliente ) throws PreencherCorretamenteOsCampos {
        try {
            List<ContatoDTO> contatos = cliente.getContatos();
            if (!contatos.get(0).getTipoContato().getTipoDeContato().equals("Email")) {
                return null;
            } else if (!contatos.get(1).getTipoContato().getTipoDeContato().equals("Telefone")) {
                return null;
            }
            ClienteEntity clienteEntity = this.salvarEEditar(cliente.converter());
            for (ContatoDTO contato : contatos) {
                ContatoEntity contatoEntity = contato.converter();
                contatoEntity.setTipoContato(tipoContatoRepository.findByTipoDeContato(contato.getTipoContato().getTipoDeContato()));
                contatoEntity.setCliente(clienteEntity);
                repository.save(contatoEntity);
            }
            return new ClienteDTO( clienteEntity );

        } catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public ClienteDTO buscarPorCpf( String cpf ) throws PreencherCorretamenteOsCampos {
        try {
            ClienteEntity clienteEntity = clienteRepository.findByCpf( cpf );
            return new ClienteDTO(clienteEntity);
        }catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public ClienteDTO buscarPorId( Integer id ) throws PreencherCorretamenteOsCampos {
        try {
            Optional<ClienteEntity> clienteEntity = clienteRepository.findById(id);
            return new ClienteDTO( clienteEntity.get() );
        } catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public List<ClienteDTO> buscarTodos() {
        ClienteEntity clienteEntity = (ClienteEntity) repository.findAll();
        return (List<ClienteDTO>) new ClienteDTO( clienteEntity );
    }
}
