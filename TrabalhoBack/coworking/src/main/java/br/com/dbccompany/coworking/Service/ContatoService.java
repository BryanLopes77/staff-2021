package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ContatoService {
    @Autowired
    private ContatoRepository repository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    public ContatoEntity salvarEEditar(ContatoEntity cliente ) {
        return repository.save( cliente );
    }

    public ContatoEntity editar( ContatoDTO contato, Integer id ) {
        try {
            ContatoEntity contatoEntity = contato.converter();
            contatoEntity.setId(id);
            return this.salvarEEditar(contatoEntity);
        }catch(Exception e){
            Log.erro404(e);
        }
        return null;
    }

    public ContatoDTO salvar(ContatoDTO contato) throws PreencherCorretamenteOsCampos {
        try{
            ContatoEntity contatoEntity = repository.save(contato.converter());
            return new ContatoDTO(contatoEntity);
        }catch (Exception e){
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public Object findAll() {
        return repository.findAll();
    }
}
