package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Exception.ObjetoInexistente;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.ConverterDinheiro;
import br.com.dbccompany.coworking.Util.ConverterTempo;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public ContratacaoDTO salvarContratacao(ContratacaoEntity contratacao) throws PreencherCorretamenteOsCampos {
        try {
            contratacao.setEspaco(espacoRepository.findById(contratacao.getEspaco().getId()).get());
            contratacao.setCliente(clienteRepository.findById(contratacao.getCliente().getId()).get());
            ContratacaoEntity cont = repository.save(contratacao);
            ContratacaoDTO contratacaoDTO = new ContratacaoDTO(cont);
            String valorFinal = calcularValor(cont);
            contratacaoDTO.setValor(valorFinal);
            return contratacaoDTO;
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ContratacaoDTO editarContratacao(ContratacaoEntity contratacao, Integer id) throws PreencherCorretamenteOsCampos {
        try {
            contratacao.setId(id);
            return  new ContratacaoDTO(salvarContratacao(contratacao).converter());
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }

    }

    public List<ContratacaoDTO> buscarAll() throws PreencherCorretamenteOsCampos {
        try {
            return converterEntityDTO(repository.findAll());
        } catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public ContratacaoDTO trazerPorId(Integer id) {
        Optional<ContratacaoEntity> contEntity = repository.findById(id);
        if (contEntity.isPresent()) {
            ContratacaoDTO contDTO = new ContratacaoDTO(contEntity.get());
            contDTO.setValor(calcularValor(contEntity.get()));
            return new ContratacaoDTO(contDTO.converter());
        }
        throw new RuntimeException();
    }

    private List<ContratacaoDTO> converterEntityDTO(List<ContratacaoEntity> contratacaoEntities) {
        List<ContratacaoDTO> dtos = new ArrayList<>();
        for (ContratacaoEntity contratacaoEntity : contratacaoEntities) {
            ContratacaoDTO dto = new ContratacaoDTO(contratacaoEntity);
            dto.setValor(calcularValor(contratacaoEntity));
            dtos.add(dto);
        }
        return dtos;
    }

    private Double diminuirValor(Double valor, Double desconto){
        if(desconto == null){
            return valor;
        }
        return valor - (valor * desconto);
    }

    private String calcularValor(ContratacaoEntity contratacao) {
        Integer quantidadeMinutos = converterTempo(contratacao);
        Double valorContratacao = contratacao.getEspaco().getValor() * quantidadeMinutos;
        Double valorComDesconto = diminuirValor(valorContratacao, contratacao.getDesconto());
        return ConverterDinheiro.converterParaSaida(valorComDesconto);
    }

    private Integer converterTempo(ContratacaoEntity contratacao) {
        TipoDeContratacaoEnum tipoContratacao = contratacao.getTipoDeContratacao();
        Integer quantidade = contratacao.getQuantidade();
        return ConverterTempo.converterTempo(quantidade, tipoContratacao);
    }
}