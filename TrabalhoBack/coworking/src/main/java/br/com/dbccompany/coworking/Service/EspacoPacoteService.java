package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {

    @Autowired
    private EspacoPacoteRepository repository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    public List<EspacoPacoteDTO> trazerTodosEspacoPacote() {
        try {
            return this.converterLista(repository.findAll());
        }catch(Exception e){
            throw new RuntimeException();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacoPacoteDTO salvar( EspacoPacoteDTO espacoPacoteDTO ) throws PreencherCorretamenteOsCampos {
        try {
            EspacoPacoteEntity espacoPacote = espacoPacoteDTO.converter();
            espacoPacote.setPacote( pacoteRepository.findById( espacoPacoteDTO.getPacote().getId() ).get() );
            espacoPacote.setEspaco( espacoRepository.findById( espacoPacoteDTO.getEspaco().getId() ).get() );
            return this.saveAndEdit(espacoPacote);
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacoPacoteDTO editar( EspacoPacoteDTO espacoPacote, Integer id ) throws PreencherCorretamenteOsCampos {
        try{
            EspacoPacoteEntity espacoPacoteEntity = espacoPacote.converter();
            espacoPacoteEntity.setId(id);
            return this.saveAndEdit( espacoPacoteEntity );
        }catch(Exception e){
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private EspacoPacoteDTO saveAndEdit( EspacoPacoteEntity espacoPacote ){
        EspacoPacoteEntity espacoPacoteNovo = repository.save( espacoPacote );
        return new EspacoPacoteDTO( espacoPacoteNovo );
    }

    public EspacoPacoteDTO findById( Integer id) throws PreencherCorretamenteOsCampos {
        try {
            Optional<EspacoPacoteEntity> espaco_Pacote = repository.findById(id);
            if (espaco_Pacote.isPresent()) {
                return new EspacoPacoteDTO(repository.findById(id).get());
            }
            return null;
        }catch( Exception e ){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private List<EspacoPacoteDTO> converterLista( Iterable<EspacoPacoteEntity> entidades ){
        ArrayList<EspacoPacoteDTO> espacoPacoteConvertidos = new ArrayList<EspacoPacoteDTO>();
        for( EspacoPacoteEntity entidade:entidades){
            espacoPacoteConvertidos.add( new EspacoPacoteDTO( entidade ) );
        }
        return espacoPacoteConvertidos;
    }
}