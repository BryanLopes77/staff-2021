package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository espacoRepository;


    @Transactional(rollbackFor = Exception.class)
    public EspacoDTO salvar( EspacoEntity espacoEntity) {
        if(espacoEntity.getNome().isEmpty()){
            throw new RuntimeException();
        }
        EspacoEntity novoEspaco = this.espacoRepository.save(espacoEntity);
        return new EspacoDTO( novoEspaco );
    }

    @Transactional(rollbackFor = Exception.class)
    public List<EspacoDTO> buscarTodos() {
        List<EspacoEntity> espacos = (List<EspacoEntity>) this.espacoRepository.findAll();
        return this.converteLista(espacos);
    }

    private List<EspacoDTO> converteLista( List<EspacoEntity> espacos ) {
        ArrayList<EspacoDTO> listaEspacos = new ArrayList<>();
        for(EspacoEntity espaco : espacos ) {
            listaEspacos.add( new EspacoDTO( espaco ) );
        }
        return listaEspacos;
    }

    public EspacoEntity findById( Integer codigoEspaco ) {
        Optional<EspacoEntity> espaco = this.espacoRepository.findById( codigoEspaco );
        if(espaco.isPresent()) {
            return espaco.get();
        }
        return null;
    }

}
