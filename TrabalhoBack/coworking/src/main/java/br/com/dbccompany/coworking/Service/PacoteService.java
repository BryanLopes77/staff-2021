package br.com.dbccompany.coworking.Service;


import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository repository;


    @Transactional(rollbackFor = Exception.class)
    public PacoteDTO salvar(PacoteEntity pacote) throws PreencherCorretamenteOsCampos {
        try{
            return new PacoteDTO(repository.save(pacote));
        }catch (Exception e){
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public PacoteDTO editar(PacoteEntity pacote, Integer id) throws PreencherCorretamenteOsCampos {
        try{
            pacote.setId(id);
            return salvar(pacote);
        }catch (Exception e){
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private List<PacoteDTO> converterEntityDTO(List<PacoteEntity> pacotes) {
        List<PacoteDTO> dtos = new ArrayList<>();
        for(PacoteEntity pacote : pacotes){
            dtos.add(new PacoteDTO(pacote));
        }
        return dtos;
    }

    public List<PacoteDTO> buscarTodos() throws PreencherCorretamenteOsCampos {
        try{
            return converterEntityDTO(repository.findAll());
        }catch (Exception e){
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public PacoteDTO buscarPorId(Integer id) throws PreencherCorretamenteOsCampos {
        try {
            Optional<PacoteEntity> pacote = repository.findById(id);
            if(pacote.isPresent()){
                return new PacoteDTO(pacote.get());
            } else return null;
        }catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

}