package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private SaldoClienteService saldoClienteService;

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;


    @Transactional(rollbackFor = Exception.class)
    public PagamentoDTO pagar (PagamentoDTO pagamentoDto) {
        pagamentoDto.setContratacao(pagamentoDto.getContratacao() != null ? new ContratacaoDTO(contratacaoRepository.findById(pagamentoDto.getContratacao().getId()).get()) : null);
        pagamentoDto.setClientePacote(pagamentoDto.getClientePacote() != null ? new ClientePacoteDTO(clientePacoteRepository.findById(pagamentoDto.getClientePacote().getId()).get()) : null);
        PagamentoEntity pagamento = pagamentoDto.converter();
        if((pagamento.getContratacao() == null && pagamento.getClientePacote() == null) || (pagamento.getContratacao() != null && pagamento.getClientePacote() != null)){
            throw new RuntimeException();
        }
        List<SaldoClienteDTO> saldosCliente = new ArrayList<>();
        if(pagamento.getContratacao() != null){
            PagamentoDTO pagamentoFinal = new PagamentoDTO(repository.save(pagamento));
            SaldoClienteEntity saldoGerado = saldoClienteService.pagar(pagamento.getContratacao());
            saldosCliente.add(new SaldoClienteDTO(saldoGerado));
            pagamentoFinal.setSaldosGerado(saldosCliente);
            return pagamentoFinal;
        }
        else{
            PagamentoDTO pagamentoFinal = new PagamentoDTO(repository.save(pagamento));
            List<SaldoClienteEntity> saldosGerados = saldoClienteService.pagarPacote(pagamento.getClientePacote());
            for(SaldoClienteEntity saldo : saldosGerados){
                saldosCliente.add(new SaldoClienteDTO(saldo));
            }
            pagamentoFinal.setSaldosGerado(saldosCliente);
            return pagamentoFinal;
        }
    }

    public List<PagamentoDTO> buscarTodos() {
        try{
            return converterToDTO(repository.findAll());
        }catch (Exception e){
            throw new RuntimeException();
        }
    }

    public PagamentoDTO buscarPorId(Integer id) throws PreencherCorretamenteOsCampos {
        try {
            Optional<PagamentoEntity> pagamento = repository.findById(id);
            if (pagamento.isPresent()){
                return new PagamentoDTO(pagamento.get());
            } else {
                return null;
            }
        }catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public List<PagamentoDTO> converterToDTO(List<PagamentoEntity> pagamentos){
        List<PagamentoDTO> listaConvertida = new ArrayList<>();
        for(PagamentoEntity pagamento : pagamentos){
            listaConvertida.add(new PagamentoDTO(pagamento));
        }
        return listaConvertida;
    }
}