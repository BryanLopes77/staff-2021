package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    SaldoClienteRepository repository;
    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    public SaldoClienteEntity editar(SaldoClienteEntity entidade, Integer idCliente, Integer idEspaco) throws PreencherCorretamenteOsCampos {
        try {
            SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
            entidade.setId(newId);
            return repository.save(entidade);
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public Optional<SaldoClienteEntity> buscarPorId(Integer idCliente, Integer idEspaco) throws PreencherCorretamenteOsCampos {
        try {
            SaldoClienteId newId = new SaldoClienteId(idCliente, idEspaco);
            Optional<SaldoClienteEntity> optionalEntity = repository.findById(newId);
            return optionalEntity;
        } catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();

        }
    }

    public SaldoClienteDTO salvar( SaldoClienteDTO dto ) {
        Optional<ClienteEntity> cliente = clienteRepository.findById( dto.getCliente().getId() );
        Optional<EspacoEntity> espaco = espacoRepository.findById( dto.getEspaco().getId() );
        SaldoClienteId id = new SaldoClienteId( dto.getCliente().getId(), dto.getEspaco().getId() );
        SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO(id);
        saldoClienteDTO.setCliente( cliente.get() );
        saldoClienteDTO.setEspaco( espaco.get() );
        this.salvarEEditar(saldoClienteDTO);
        return new SaldoClienteDTO( saldoClienteDTO );
    }

    private SaldoClienteEntity salvarEEditar( SaldoClienteEntity saldoCliente ){
        return repository.save( saldoCliente );
    }

    public SaldoClienteEntity pagar(ContratacaoEntity contratacao) {
        SaldoClienteId id = new SaldoClienteId();
        id.setId_espaco(contratacao.getEspaco().getId());
        id.setId_cliente(contratacao.getCliente().getId());
        if(saldoClienteIsPresent(id)){
            SaldoClienteEntity saldoCliente = repository.findById(id).get();
            saldoCliente.setQuantidade(saldoCliente.getQuantidade() + contratacao.getQuantidade());
            saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays((Integer)contratacao.getPrazo()));
            return repository.save(saldoCliente);

        } else{
            SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
            saldoCliente.setId(id);
            saldoCliente.setCliente(contratacao.getCliente());
            saldoCliente.setEspaco(contratacao.getEspaco());
            saldoCliente.setTipoDeContratacao(contratacao.getTipoDeContratacao());
            saldoCliente.setQuantidade(contratacao.getQuantidade());
            LocalDate vencimento = LocalDate.now().plusDays(contratacao.getPrazo());
            saldoCliente.setVencimento(vencimento);
            return repository.save(saldoCliente);
        }
    }

    private Boolean saldoClienteIsPresent(SaldoClienteId id){
        return repository.findById(id).isPresent();
    }

    public List<SaldoClienteEntity> pagarPacote(ClientePacoteEntity clientePacote) {
        List<SaldoClienteEntity> saldosCliente = new ArrayList<>();
        Integer quantidade = clientePacote.getQuantidade();
        ClienteEntity cliente = clientePacote.getCliente();
        List<EspacoPacoteEntity> espacoPacotes = clientePacote.getPacote().getEspacoPacoteEntities();
        for(EspacoPacoteEntity espacoPacote : espacoPacotes){
            SaldoClienteId id = new SaldoClienteId();
            id.setId_cliente(cliente.getId());
            id.setId_espaco(espacoPacote.getEspaco().getId());
            if(saldoClienteIsPresent(id)){
                SaldoClienteEntity saldoCliente = repository.findById(id).get();
                saldoCliente.setQuantidade(saldoCliente.getQuantidade() + (quantidade * espacoPacote.getQuantidade()));
                saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays((Integer)(quantidade * espacoPacote.getPrazo())));
                saldosCliente.add(repository.save(saldoCliente));
            }
            else{
                SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
                saldoCliente.setId(id);
                saldoCliente.setCliente(cliente);
                saldoCliente.setEspaco(espacoPacote.getEspaco());
                saldoCliente.setTipoDeContratacao(espacoPacote.getTipoContratacao());
                saldoCliente.setQuantidade(quantidade * espacoPacote.getQuantidade());
                saldoCliente.setVencimento(LocalDate.now().plusDays((Integer)(quantidade * espacoPacote.getPrazo())));
                saldosCliente.add(repository.save(saldoCliente));
            }
        }
        return saldosCliente;
    }

    public List<SaldoClienteDTO> trazerTodos() throws PreencherCorretamenteOsCampos {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private List<SaldoClienteDTO> convertList(Iterable<SaldoClienteEntity> entidades ){
        ArrayList<SaldoClienteDTO> listaConvertida = new ArrayList<SaldoClienteDTO>();
        for( SaldoClienteEntity entidade:entidades){
            listaConvertida.add( new SaldoClienteDTO( entidade ) );
        }
        return listaConvertida;
    }
}
