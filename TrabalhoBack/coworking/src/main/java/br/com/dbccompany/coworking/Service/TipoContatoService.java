package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.PreencherCorretamenteOsCampos;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    public List<TipoContatoDTO> trazerTodosOsItens() throws PreencherCorretamenteOsCampos {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContatoDTO salvar( TipoContatoDTO tipoContato ) throws PreencherCorretamenteOsCampos {
        try {
            return this.salvarEEditar( tipoContato.converter() );
        } catch (Exception e) {
            Log.erro400(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContatoDTO editar( TipoContatoDTO tipoContato, Integer id ) throws PreencherCorretamenteOsCampos {
        try {
            TipoContatoEntity tipoContatoEntity = tipoContato.converter();
            tipoContatoEntity.setId(id);
            return this.salvarEEditar(tipoContatoEntity);
        } catch (Exception e) {
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    public TipoContatoDTO salvarEEditar( TipoContatoEntity tipoContato ){
        TipoContatoEntity tipoNovo = repository.save( tipoContato );
        return new TipoContatoDTO( tipoNovo );
    }

    public TipoContatoDTO buscarPorId( Integer id) throws PreencherCorretamenteOsCampos {
        try {
            Optional<TipoContatoEntity> tipoContato = repository.findById(id);
            if ( tipoContato.isPresent()) {
                return new TipoContatoDTO(repository.findById(id).get());
            }
            return null;
        }catch( Exception e ){
            Log.erro404(e);
            throw new PreencherCorretamenteOsCampos();
        }
    }

    private List<TipoContatoDTO> convertList(Iterable<TipoContatoEntity> entidades){
        ArrayList<TipoContatoDTO> tiposConvertidos = new ArrayList<TipoContatoDTO>();
        for( TipoContatoEntity entidade:entidades){
            tiposConvertidos.add( new TipoContatoDTO( entidade ) );
        }
        return tiposConvertidos;
    }

}