package br.com.dbccompany.coworking.Util;

public class ConverterDinheiro {

    public static Double converterParaEntrada(String valor) {
        String entrada = valor.replace("R$", "").replace(".", "").replace(" ", "").replace(",", ".");
        return Double.parseDouble(entrada);
    }

    public static String converterParaSaida(Double valor){
        StringBuilder saida = new StringBuilder();
        saida.append("R$ ");
        saida.append(valor);
        return saida.toString().replace(".", ",");
    }
}

