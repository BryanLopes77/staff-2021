package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;

import java.time.Duration;

public class ConverterTempo {

    public static Integer converterTempo(Integer quantidade, TipoDeContratacaoEnum tipoDeContratacao){
        Integer qtd = 0;
        switch (tipoDeContratacao) {
            case MINUTO:
                qtd = quantidade;
            case HORA:
                qtd = quantidade * 60;
            case TURNO:
                qtd = quantidade * 60 * 5;
            case DIARIA:
                qtd = quantidade * 60 * 5 * 2;
            case MES:
                qtd = quantidade * 60 * 5 * 2 * 5 * 4;
        }
        return qtd;
    }
}
