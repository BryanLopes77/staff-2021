package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@Profile("test")
public class AcessoRepositoryTest {

    @Autowired
    private AcessoRepository repository;
    @Autowired
    private SaldoClienteRepository saldoClienteRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;
    @Autowired
    private EspacoRepository espacoRepository;

    @Test
    public void salvarAcesso(){
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        TipoContatoEntity tipoContatoEntity1 = new TipoContatoEntity();
        tipoContatoEntity.setTipoDeContato("telefone");
        tipoContatoEntity1.setTipoDeContato("email");
        tipoContatoRepository.save(tipoContatoEntity);
        tipoContatoRepository.save(tipoContatoEntity1);
        ContatoEntity contatoEntity = new ContatoEntity();
        ContatoEntity contatoEntity1 = new ContatoEntity();
        contatoEntity.setTipoContato(tipoContatoEntity);
        contatoEntity1.setTipoContato(tipoContatoEntity1);
        contatoEntity.setValor("996846808");
        contatoEntity1.setValor("bryan@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contatoEntity);
        contatos.add(contatoEntity1);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Bryan");
        String cpf = "85883387004";
        cliente.setCpf(cpf);
        cliente.setDataDeNascimento(Date.from(Instant.ofEpochSecond(2000-10-10)));
        ClienteEntity clienteSalvo = clienteRepository.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Luiz");
        espaco.setValor(10.0);
        espaco.setQtdPessoas(4554);
        EspacoEntity espacoSalvo = espacoRepository.save(espaco);

        SaldoClienteEntity saldo = new SaldoClienteEntity();
        SaldoClienteId id = new SaldoClienteId();
        id.setId_cliente(clienteSalvo.getId());
        id.setId_espaco(espacoSalvo.getId());
        saldo.setId(id);
        saldo.setCliente(clienteSalvo);
        saldo.setEspaco(espacoSalvo);
        saldo.setTipoDeContratacao(TipoDeContratacaoEnum.HORA);
        saldo.setQuantidade(20);
        LocalDate dataAtual = LocalDate.now();
        saldo.setVencimento(dataAtual);
        saldoClienteRepository.save(saldo);

        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldo);
        acesso.setEntrada(true);
        acesso.setExcecao(false);
        LocalDateTime dataAcesso = LocalDateTime.now();
        acesso.setData(dataAcesso);
        repository.save(acesso);
        assertEquals(acesso.getData(), repository.findById(1).get().getData());
    }
}
