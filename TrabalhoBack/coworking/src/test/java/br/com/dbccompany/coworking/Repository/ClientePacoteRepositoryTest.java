package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ClientePacoteRepositoryTest {
    @Autowired
    public ClientePacoteRepository repository;
    @Autowired
    public ClienteRepository clienteRepository;
    @Autowired
    public PacoteRepository pacoteRepository;

    @Test
    public void salvarClientePacote(){
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        tipoContatoEntity.setId(1);
        tipoContatoEntity.setTipoDeContato("E-mail");
        contato.setId(1);
        contato.setTipoContato(tipoContatoEntity);
        contato.setValor("bryan@gmail.com");

        ContatoEntity contato2 = new ContatoEntity();
        TipoContatoEntity tipoContatoEntity2 = new TipoContatoEntity();
        tipoContatoEntity2.setId(2);
        tipoContatoEntity2.setTipoDeContato("Telefone");
        contato2.setId(2);
        contato2.setTipoContato(tipoContatoEntity2);
        contato2.setValor("989412704");

        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contato);
        contatos.add(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(1);
        cliente.setDataDeNascimento(Date.from(Instant.ofEpochSecond(2010-10-10)));
        cliente.setCpf("85883387004");
        cliente.setNome("Bryan");
        cliente.setContatos(contatos);
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(20.0);
        pacote.setId(1);
        pacoteRepository.save(pacote);

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setId(1);
        clientePacote.setQuantidade(1);
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        repository.save(clientePacote);
        assertEquals(1, repository.findByPacote(pacote).get(0).getQuantidade());
        assertEquals(2, repository.findByCliente(cliente).get(0).getCliente().getContatos().size());
    }
}
