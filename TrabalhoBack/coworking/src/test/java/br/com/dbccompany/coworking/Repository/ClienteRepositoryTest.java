package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@Profile("test")
public class ClienteRepositoryTest {
    @Autowired
    private ClienteRepository repository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarCliente(){
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        TipoContatoEntity tipoContatoEntity1 = new TipoContatoEntity();
        tipoContatoEntity.setTipoDeContato("Email");
        tipoContatoEntity1.setTipoDeContato("Telefone");
        tipoContatoRepository.save(tipoContatoEntity);
        tipoContatoRepository.save(tipoContatoEntity1);

        ContatoEntity contatoEntity = new ContatoEntity();
        ContatoEntity contatoEntity1 = new ContatoEntity();
        contatoEntity.setTipoContato(tipoContatoEntity);
        contatoEntity1.setTipoContato(tipoContatoEntity1);
        contatoEntity.setValor("bryan@gmail.com");
        contatoEntity1.setValor("34526440");

        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contatoEntity);
        contatos.add(contatoEntity1);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Bryan");
        cliente.setDataDeNascimento(Date.from(Instant.ofEpochSecond(2000-10-10)));
        String cpf = "85883387004";
        cliente.setCpf(cpf);
        repository.save(cliente);

        assertEquals("Bryan", repository.findByNome(cliente.getNome()).getNome());
    }

    @Test
    public void editarCliente(){
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        TipoContatoEntity tipoContatoEntity1 = new TipoContatoEntity();
        tipoContatoEntity.setTipoDeContato("Email");
        tipoContatoEntity1.setTipoDeContato("Telefone");
        tipoContatoRepository.save(tipoContatoEntity);
        tipoContatoRepository.save(tipoContatoEntity1);

        ContatoEntity contatoEntity = new ContatoEntity();
        ContatoEntity contatoEntity1 = new ContatoEntity();
        contatoEntity.setTipoContato(tipoContatoEntity);
        contatoEntity1.setTipoContato(tipoContatoEntity1);
        contatoEntity.setValor("bryan@gmail.com");
        contatoEntity1.setValor("996846808");

        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contatoEntity);
        contatos.add(contatoEntity1);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Tamires");
        cliente.setDataDeNascimento(Date.from(Instant.ofEpochSecond(2000-10-10)));
        String cpf = "85883387004";
        cliente.setCpf(cpf);
        repository.save(cliente);

        cliente.setDataDeNascimento(Date.from(Instant.ofEpochSecond(2010-10-20)));
        repository.save(cliente);
        assertEquals(cliente.getDataDeNascimento(), repository.findByDataDeNascimento(cliente.getDataDeNascimento()).getDataDeNascimento());
    }
}

