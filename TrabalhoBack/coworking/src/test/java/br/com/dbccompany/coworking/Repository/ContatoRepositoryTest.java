package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Profile("test")
public class ContatoRepositoryTest {
    @Autowired
    private ContatoRepository repository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarContato(){
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setTipoDeContato("email");
        tipoContato = tipoContatoRepository.save(tipoContato);
        contato.setTipoContato(tipoContato);
        contato.setValor("bryan@gmail.com");
        repository.save(contato);
        assertEquals( contato.getValor(), (repository.findByValor("bryan@gmail.com")).getValor() );
    }
}
