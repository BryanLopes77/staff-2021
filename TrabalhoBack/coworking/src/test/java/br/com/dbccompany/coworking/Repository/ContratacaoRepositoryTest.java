package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Profile("test")
public class ContratacaoRepositoryTest {
    @Autowired
    private ContratacaoRepository repository;

    @Test
    public void salvarContrato() {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setPrazo(5);
        contratacao.setQuantidade(7);
        contratacao.setTipoDeContratacao(TipoDeContratacaoEnum.MES);
        contratacao = repository.save(contratacao);
        assertEquals(contratacao.getTipoDeContratacao(), TipoDeContratacaoEnum.MES);
        assertEquals(contratacao.getPrazo(), 5);
        assertEquals(contratacao.getQuantidade(), 7);
    }
}
