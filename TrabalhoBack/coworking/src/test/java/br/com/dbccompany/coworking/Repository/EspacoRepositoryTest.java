package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Profile("test")
public class EspacoRepositoryTest {
    @Autowired
    private EspacoRepository repository;

    @Test
    public void salvar(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setValor(15.0);
        espaco.setNome("Sala");
        espaco.setQtdPessoas(20);
        repository.save(espaco);
        assertEquals(espaco.getNome(), repository.findByNome(espaco.getNome()).getNome());
    }
}
