package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PacoteRepositoryTest {

    @Autowired
    private PacoteRepository repository;

    @Autowired EspacoRepository espacoRepository;

    @Test
    public void salvarPacote(){
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setValor(2.6);
        espacoEntity.setNome("Cozinha");
        espacoEntity.setQtdPessoas(7);
        EspacoEntity espacoEntity1 = new EspacoEntity();
        espacoEntity1.setValor(2.6);
        espacoEntity1.setNome("Banheiro");
        espacoEntity1.setQtdPessoas(3);
        espacoRepository.save(espacoEntity);
        espacoRepository.save(espacoEntity1);
        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();
        espacoPacoteEntity.setEspaco(espacoEntity);
        espacoPacoteEntity.setTipoContratacao(TipoDeContratacaoEnum.DIARIA);
        espacoPacoteEntity.setQuantidade(20);
        espacoPacoteEntity.setPrazo(100);
        EspacoPacoteEntity espacoPacoteEntity1 = new EspacoPacoteEntity();
        espacoPacoteEntity1.setEspaco(espacoEntity1);
        espacoPacoteEntity1.setQuantidade(12);
        espacoPacoteEntity1.setTipoContratacao(TipoDeContratacaoEnum.HORA);
        espacoPacoteEntity1.setPrazo(50);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(espacoPacoteEntity);
        espacos.add(espacoPacoteEntity1);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(4000.0);
        pacote.setEspacoPacoteEntities(espacos);
        repository.save(pacote);
        repository.findAll();
        assertEquals(pacote.getValor(), repository.findByValor(pacote.getValor()).getValor());
    }

    @Test
    public void editarEspaco(){
        EspacoEntity espacoEntity = new EspacoEntity();
        espacoEntity.setValor(3.2);
        espacoEntity.setNome("Quiosque");
        espacoEntity.setQtdPessoas(340);
        EspacoEntity espacoEntity1 = new EspacoEntity();
        espacoEntity1.setValor(4.2);
        espacoEntity1.setNome("Hotel");
        espacoEntity1.setQtdPessoas(566);
        espacoRepository.save(espacoEntity);
        espacoRepository.save(espacoEntity1);
        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();
        espacoPacoteEntity.setEspaco(espacoEntity);
        espacoPacoteEntity.setTipoContratacao(TipoDeContratacaoEnum.DIARIA);
        espacoPacoteEntity.setQuantidade(23);
        espacoPacoteEntity.setPrazo(40);
        EspacoPacoteEntity espacoPacoteEntity1 = new EspacoPacoteEntity();
        espacoPacoteEntity1.setEspaco(espacoEntity1);
        espacoPacoteEntity1.setQuantidade(34);
        espacoPacoteEntity1.setTipoContratacao(TipoDeContratacaoEnum.MES);
        espacoPacoteEntity1.setPrazo(23);
        List<EspacoPacoteEntity> espacos = new ArrayList<>();
        espacos.add(espacoPacoteEntity);
        espacos.add(espacoPacoteEntity1);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacoteEntities(espacos);
        PacoteEntity salvo = repository.save(pacote);
        pacote.setValor(400.0);
        pacote.setId(salvo.getId());
        repository.save(pacote);
        assertEquals(pacote.getValor(), repository.findById(salvo.getId()).get().getValor());
    }

}
