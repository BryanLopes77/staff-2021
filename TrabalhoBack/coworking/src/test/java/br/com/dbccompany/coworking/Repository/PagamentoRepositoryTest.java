package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Entity.Enum.TipoDePagamentoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class PagamentoRepositoryTest {

    @Autowired
    PagamentoRepository repository;
    @Autowired
    ClientePacoteRepository clientePacoteRepository;
    @Autowired
    ContratacaoRepository contratacaoRepository;
    @Autowired
    EspacoRepository espacoRepository;
    @Autowired
    ClienteRepository clienteRepository;
    @Autowired
    PacoteRepository pacoteRepository;

    /*@Test
    public void salvarContratacao() {
        //TODO: ERRO EM CONTATOENTITY.VALOR
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("bryan");
        cliente.setCpf("85883387004");
        cliente.setDataDeNascimento(new Date(2000, 10, 10));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Churrasqueira");
        espaco.setQtdPessoas(20);
        espaco.setValor(350.00);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteRepository.findByCpf("85883387004"));
        contratacao.setEspaco(espacoRepository.findByNome("Churrasqueira"));
        contratacao.setPrazo(20);
        contratacao.setDesconto(80.00);
        contratacao.setQuantidade(60);
        contratacao.setTipoDeContratacao(TipoDeContratacaoEnum.HORA);
        contratacaoRepository.save(contratacao);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(350.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacotes.get(0));
        clientePacote.setCliente(clienteRepository.findByCpf("85883387004"));
        clientePacote.setQuantidade(60);
        clientePacoteRepository.save(clientePacote);


        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setClientePacote(clientePacoteRepository.findByCliente(clienteRepository.findByCpf("85883387004")).get(0));
        pagamento.setContratacao(contratacaoRepository.findAllByCliente(clienteRepository.findByCpf("85883387004")).get(0));
        pagamento.setTipoDePagamento(TipoDePagamentoEnum.DEBITO);
        repository.save(pagamento);

        assertEquals(TipoDePagamentoEnum.DEBITO, repository.findAllByClientePacote(clientePacoteRepository
                .findByCliente(clienteRepository.findByCpf("85883387004")).get(0)).get(0).getTipoDePagamento());
        assertEquals(TipoDePagamentoEnum.DEBITO, repository.findAllByContratacao(contratacaoRepository
                .findAllByCliente(clienteRepository.findByCpf("85883387004")).get(0)).get(0).getTipoDePagamento());
        assertEquals("85883387004", repository.findByTipoDePagamento(TipoDePagamentoEnum.DEBITO)
                .getClientePacote().getCliente().getCpf());

    }*/

}