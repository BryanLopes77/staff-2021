package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Enum.TipoDeContratacaoEnum;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class SaldoClienteRepositoryTest {

    @Autowired
    private SaldoClienteRepository repository;
    @Autowired
    private ClienteRepository repositoryCliente;
    @Autowired
    private EspacoRepository repositoryEspaco;

    /*@Test
    public void salvarSaldoPagamento() {
        //TODO: ADICIONAR ATRIBUTOS DE CONTATO;
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("Bryan");
        cliente.setCpf("85883387004");
        cliente.setDataDeNascimento(Date.from(Instant.ofEpochSecond(10-11-2000)));
        repositoryCliente.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Casa");
        espaco.setValor(15000.00);
        espaco.setQtdPessoas(35);
        repositoryEspaco.save(espaco);
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(new SaldoClienteId(cliente.getId(), espaco.getId()));
        saldoCliente.setTipoDeContratacao(TipoDeContratacaoEnum.DIARIA);
        saldoCliente.setQuantidade(30);
        saldoCliente.setVencimento(LocalDate.now());
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        repository.save(saldoCliente);
        assertEquals(saldoCliente.getCliente(), repository.findById(saldoCliente.getId()).get().getCliente());
    }*/

}
