package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class TipoContatoRepositoryTest {
    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salvarTipoContato(){
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        tipoContatoEntity.setTipoDeContato("telefone");
        repository.save(tipoContatoEntity);
        assertEquals(tipoContatoEntity.getTipoDeContato(), repository.findByTipoDeContato(tipoContatoEntity.getTipoDeContato()).getTipoDeContato());
    }

    @Test
    public void editar(){
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        tipoContatoEntity.setTipoDeContato("telefone");
        repository.save(tipoContatoEntity);
        TipoContatoEntity tipoContatoEntity1 = new TipoContatoEntity();
        tipoContatoEntity1.setTipoDeContato("email");
        tipoContatoEntity1.setId(1);
        repository.save(tipoContatoEntity1);
        assertEquals(tipoContatoEntity1.getTipoDeContato(), repository.findById(1).get().getTipoDeContato());
    }

    @Test
    public void buscarTodos(){
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        TipoContatoEntity tipoContatoEntity1 = new TipoContatoEntity();
        tipoContatoEntity.setTipoDeContato("Carta");
        tipoContatoEntity1.setTipoDeContato("Linkedin");
        repository.save(tipoContatoEntity);
        repository.save(tipoContatoEntity1);
        List<TipoContatoEntity> list = new ArrayList<>();
        list.add(tipoContatoEntity);
        list.add(tipoContatoEntity1);
        assertEquals(list, repository.findAll());
    }
}
