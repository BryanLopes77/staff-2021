package br.com.dbccompany.logcoworking.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/Log")
public class LogController {
    @Autowired
    LogService logsService;

    private Logger logger = LoggerFactory.getLogger(LogsApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogDTO> todosLogs() {
        return logsService.buscarTodos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public LogDTO salvar(@RequestBody LogDTO logsDTO){
        return logsService.salvarLog(logsDTO);
    }

    @GetMapping(value = "/buscar/{id}")
    @ResponseBody
    public LogDTO buscarPorId(@PathVariable String id) {
        return logsService.porId(id);
    }
}