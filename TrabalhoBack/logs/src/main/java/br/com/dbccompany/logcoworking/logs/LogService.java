package br.com.dbccompany.logcoworking.logs;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    LogRepository logsRepository;

    private Logger logger = LoggerFactory.getLogger( LogsApplication.class );

    @Transactional
    public LogDTO salvarLog(LogDTO logsDTO) {
        try {
            LogEntity logFinal = logsDTO.converter();
            LogDTO newDTO = new LogDTO(logsRepository.save(logFinal));
            return newDTO;
        } catch (Exception e) {
            logger.error("Erro ao salvar log: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional
    public List<LogDTO> buscarTodos() {
        try {
            List<LogDTO> logsDTOS = new ArrayList<>();
            for (LogEntity logEntity: logsRepository.findAll()) {
                logsDTOS.add(new LogDTO(logEntity));
            }
            return logsDTOS;
        } catch (Exception e) {
            logger.error("Erro ao buscar logs: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    public LogDTO porId(String id) {
        try {
            return new LogDTO(logsRepository.findById(id).get());
        } catch (Exception e) {
            logger.error("Não foi possível encontrar o log: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
