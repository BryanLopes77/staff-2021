import React from 'react';
import { Link } from 'react-router-dom';

import '../button/button.css';

const Button = ({ nome, link }) => 
  <React.Fragment>
    <button>
        <div className="box-3">
            <div className="btn btn-three">
                { link ? <Link to={ link } >{ nome }</Link> : nome }
            </div>    
        </div>
    </button>    
  </React.Fragment>

export default Button;