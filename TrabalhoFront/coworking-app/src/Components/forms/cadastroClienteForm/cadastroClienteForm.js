import React from 'react';
import Form from '../../formAntd/';
import Input from '../../inputAntd/';

import './cadastroClienteForm.css';

export default class CadastroClienteForm extends React.Component {

    cadastro = ( c ) => {
        const { cadastrar } = this.props;
        cadastrar( c );
    }

    render() {
        const { mensagem } = this.props;

        return (
            <React.Fragment>
                <Form onFinish={ this.cadastro } className="cadastro-cliente" style={{ fontFamily: "Arial", fontSize: 20, color: 'white' }}>
                    <Form.Item style={{ margin: 30, marginTop: 280 }}  
                        label="Nome:" 
                        name="nome" 
                        rules={[{ required: true, message: "*Digite seu nome"  
                        }]}>
                        <Input style={{ height: 30, width: 500, fontSize: 20 }}
                            placeholder="Exemplo Nome"
                        />
                    </Form.Item>
                    <Form.Item style={{ margin: 30 }}  
                        label="CPF:" 
                        name="cpf" 
                        rules={[{ required: true, message: "*Digite seu CPF" 
                        }]}>
                        <Input style={{ height: 30, width: 500, fontSize: 20 }}
                            placeholder="99999999999"
                        />
                    </Form.Item>
                    <Form.Item style={{ margin: 30 }}  
                        label="Email:" 
                        name="email" 
                        rules={[{ required: true, message: "*Digite seu Email" 
                        }]}>
                        <Input style={{ height: 30, width: 500, fontSize: 20 }}
                            placeholder="exemplo@gmail.com"
                        />
                    </Form.Item>
                    <Form.Item style={{ margin: 30 }}  
                        label="Telefone:" 
                        name="telefone" 
                        rules={[{ required: true, message: "*Digite seu telefone" 
                        }]}>
                        <Input style={{ height: 30, width: 500, fontSize: 20 }}
                            placeholder="999999999"
                        />
                    </Form.Item>
                    <Form.Item style={{ margin: 30 }}  
                        label="Data de nascimento:" 
                        name="dataNascimento" 
                        rules={[{ required: true, message: "*Digite sua data de nascimento"
                        }]}>
                        <Input style={{ height: 30, width: 500, fontSize: 20 }} 
                            placeholder="AAAA-MM-DD"
                        />
                    </Form.Item>
                    <Form.Item>
                        <button className="btn-cliente" type="primary">Cadastrar</button>
                    </Form.Item>
                    { mensagem === "Cadastro realizado!" &&
                    <span className="exito">Cadastro realizado com sucesso!</span> 
                    }
    
                    { mensagem === "Erro ao cadastrar" && 
                    <span className="erro-espaco">Erro ao cadastrar o espaço!</span>
                    }
                </Form>
            </React.Fragment>
        )    
    }
}
