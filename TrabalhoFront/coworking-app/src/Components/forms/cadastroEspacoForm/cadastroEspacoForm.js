import React from 'react';
import Form from '../../formAntd/';
import Input from '../../inputAntd/';

import './cadastroEspacoForm.css';

export default class CadastroEspaco extends React.Component {

    cadastro = ( c ) => {
        const { cadastrar } = this.props;
        cadastrar( c );
    }

    render() {

        const { mensagem } = this.props;

        return (
            <React.Fragment>
                <Form className="cadastro-espaco" onFinish={ this.cadastro }>
                    <Form.Item 
                        label="Nome:" 
                        name="nome" 
                        rules={[{ required: true, message: "Digite o nome do espaço" 
                        }]}>
                        <Input
                            placeholder="Ex.: Mesa sala 2"
                        />
                    </Form.Item>
                    <Form.Item 
                        label="Quantidade de pessoas:" 
                        name="qtdPessoas" 
                        rules={[{ required: true, message: "Digite o número de pessoas" 
                        }]}>
                        <Input
                            placeholder="0"
                        />
                    </Form.Item>
                    <Form.Item 
                        label="Valor:" 
                        name="valor" 
                        rules={[{ required: true, message: "Digite o valor" 
                        }]}>
                        <Input
                            placeholder="0.0"
                        />
                    </Form.Item>
                    <Form.Item>
                        <button className="btn-cadastro" type="primary">Cadastrar</button>
                    </Form.Item>
                    { mensagem === "Cadastro realizado!" &&
                    <span className="exito">Cadastro realizado com sucesso!</span> 
                    }
    
                    { mensagem === "Erro ao cadastrar" && 
                    <span className="erro-espaco">Erro ao cadastrar o espaço!</span>
                    }
                </Form>
            </React.Fragment>
        )    
    }

}
