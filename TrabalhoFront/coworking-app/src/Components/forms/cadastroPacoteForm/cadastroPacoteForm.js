import React from 'react';
import Form from '../../formAntd/';
import Input from '../../inputAntd/';

import './cadastroPacoteForm.css';

export default class CadastroPacoteForm extends React.Component {

    cadastro = ( c ) => {
        const { cadastrar } = this.props;
        cadastrar( c );
    }

    render() {

        const { mensagem } = this.props;

        return (
            <React.Fragment>
                <Form className="cadastro-pacote" onFinish={ this.cadastro }>
                    <Form.Item 
                        label="Valor do pacote:" 
                        name="valor" 
                        rules={[{ required: true, message: "Digite o valor do pacote" 
                        }]}>
                        <Input
                            placeholder="0.0"
                        />
                    </Form.Item>
                    <Form.Item>
                        <button className="btn-pacote" type="primary">Cadastrar</button>
                    </Form.Item>
                    { mensagem === "Cadastro realizado!" &&
                    <span className="exito">Pacote cadastrado!</span> 
                    }
                    { mensagem === "Erro ao cadastrar" && 
                    <span className="erro-pacote">Erro ao cadastrar o pacote!</span>
                    }
                </Form>
            </React.Fragment>
        )    
    }

}