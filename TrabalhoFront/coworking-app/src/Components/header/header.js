import React from 'react';
import './header.css';
import { Link } from 'react-router-dom';
import Logo from '/home/bryan/Área de Trabalho/staff-2021/TrabalhoFront/coworking-app/src/assets/img/blc.png';
import Navigation from '../navigation';

const Header = ({ link, nome }) =>
    <React.Fragment>
        <header className="header">
            <div className="logo">
                <Link to="/"><img className="hdr-img" src={Logo} alt="Logo" /></Link>
            </div>
            <Navigation link={{ pathname:"/" }} nome="Início" />
            <Navigation link={{ pathname:"/sobre" }} nome="Sobre" />
            <Navigation link={{ pathname:"/contato" }} nome="Contato" />
            <Navigation link={{ pathname:"/cliente" }} nome="Cliente" />
        </header>
    </React.Fragment>
export default Header;