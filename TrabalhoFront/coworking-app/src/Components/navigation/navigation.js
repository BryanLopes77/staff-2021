import React from 'react';
import { Link } from 'react-router-dom';

import './navigation.css';

const Navigation = ({ link, nome }) =>
    <React.Fragment>
        <ul>
            <li>
                <Link to={link} className="link-header">{ nome }</Link>
            </li>
        </ul>
    </React.Fragment>

export default Navigation;