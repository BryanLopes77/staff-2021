import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from '../Containers/App';
import Customer from '../Containers/customer'
import CadastroCliente from './cadastroCliente';
import CadastroEspaco from './cadastroEspaco';
import CadastroPacote from './cadastroPacote/cadastroPacote';
import MenuCliente from './menuCliente';

export default class Rotas extends React.Component {
    render(){
        return (
          <Router>
            <Route path="/" exact component={ App } />
            <Route path="/cliente" exact component={ Customer } />
            <Route path="/cadastro/cliente" exact component={ CadastroCliente } />
            <Route path="/cadastro/espaco" exact component={ CadastroEspaco } />
            <Route path="/cadastro/pacote" exact component={ CadastroPacote } />
            <Route path="/menuCliente" exact component={ MenuCliente } />
          </Router>
        );
      }
}