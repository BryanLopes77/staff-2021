import React from 'react';
import Api from '../../Models/Api'
import Cliente from '../../Models/Cliente';
import Header from '../../Components/header'
import CadastroClienteForm from '../../Components/forms/cadastroClienteForm/';
import User from '../../assets/img/user.png';

import './cadastroCliente.css';

export default class CadastroCliente extends React.Component {
    constructor( props ) {
        super( props );
        this.Api = new Api();
        this.state = {
            mensagem: ''
        }
    }

    salvarCliente = ( cliente ) => {
        let novoCliente = new Cliente( cliente );
        this.Api.salvarCliente( novoCliente ).then( esp => {
            this.setState( ( state ) => {
                return {
                    ...state,
                    mensagem: "Cadastro realizado!"
                }
            } ) 
        } ).catch( esp => {
            this.setState( ( state ) => {
                return {
                    ...state,
                    mensagem: "Erro ao cadastrar"
                }
            } )
        } )
    }

    render() {
        return (
            <React.Fragment>
                <Header/>
                <CadastroClienteForm cadastrar={ this.salvarCliente.bind( this ) }/>
                <img className="usr-img" src={ User } alt="" /> 
            </React.Fragment>
        )
    }
}