import React from 'react';
import Api from '../../Models/Api';
import Espaco from '../../Models/Espaco';
import Header from '../../Components/header';
import CadastroEspacoForm from '../../Components/forms/cadastroEspacoForm';


export default class CadastroEspaco extends React.Component {
    constructor( props ) {
        super( props );
        this.Api = new Api();
        this.state = {
            mensagem: ''
        }
    }

    salvarEspaco = ( espaco ) => {
        let novoEspaco = new Espaco( espaco );
        this.Api.salvarEspaco( novoEspaco ).then( esp => {
            this.setState( ( state ) => {
                return {
                    ...state,
                    mensagem: "Cadastro realizado!"
                }
            } ) 
        } ).catch( esp => {
            this.setState( ( state ) => {
                return {
                    ...state,
                    mensagem: "Erro ao cadastrar"
                }
            } )
        } )
    }

    render() {
        return (
            <React.Fragment>
                <Header/>
                <CadastroEspacoForm cadastrar={ this.salvarEspaco.bind( this ) } mensagem={ this.state.mensagem } />
        </React.Fragment>
        );
    }
}