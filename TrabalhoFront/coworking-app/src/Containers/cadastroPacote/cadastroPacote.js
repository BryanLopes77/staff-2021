import React from 'react';
import Api from '../../Models/Api';
import Pacote from '../../Models/Pacote';
import CadastroPacoteForm from '../../Components/forms/cadastroPacoteForm';
import Header from '../../Components/header';

export default class CadastroPacote extends React.Component {
    constructor( props ) {
        super( props );
        this.Api = new Api();
        this.state = {
            mensagem: ''
        }
    }

    salvarPacote = ( pacote ) => {
        let novoPacote = new Pacote( pacote );
        this.Api.salvarPacote( novoPacote ).then( esp => {
            this.setState( ( state ) => {
                return {
                    ...state,
                    mensagem: "Cadastro realizado!"
                }
            } ) 
        } ).catch( esp => {
            this.setState( ( state ) => {
                return {
                    ...state,
                    mensagem: "Erro ao cadastrar"
                }
            } )
        } )
    }

    render() {
        return (
            <React.Fragment>
                <Header/>
                <CadastroPacoteForm cadastrar={ this.salvarPacote.bind( this ) } mensagem={ this.state.mensagem } />
        </React.Fragment>
        );
    }
}