import React from 'react';
import './customer.css';
import Header from '../../Components/header';
import Button from '../../Components/button';
import Content from '../../Components/contentAntd';
import Row from '../../Components/rowAntd';
import Col from '../../Components/colAntd';

export default class Customer extends React.Component {

  render() {
    return (
      <div className="Customer">
        <Header />
        <Content className="cont">
            <Row>
                <Col span={16}>
                    <h2 className="titulo-cstmr">Venha fazer parte do time BLC!</h2>
                    <div className="opcoes-cstmr">
                        <Button link="/cadastro/cliente" nome="Seja cliente" />  
                        <Button link="/cadastro/espaco" nome="Cadastre um espaço" />
                        <Button link="/cadastro/pacote" nome="Contrate um pacote" />
                        <Button link="/cadastro/espacoPacote" nome="Finalize sua contratação" />
                    </div>    
                </Col>
            </Row>    
        </Content>
      </div>
    );
  }  
}