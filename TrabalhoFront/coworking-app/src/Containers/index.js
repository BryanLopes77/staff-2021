import Home from './home';
import Interna from './interna';
import About from './about';
import Contact from './contact';

export { Home, Interna, About, Contact };