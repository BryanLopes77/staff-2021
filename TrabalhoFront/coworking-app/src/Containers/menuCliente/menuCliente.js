import React from 'react';
import Header from '../../Components/header';
import './menuCliente.css';

export default class MenuCliente extends React.Component {

  render() {
    return (
      <div>
        <Header />
        <h2>Seja bem-vindo(a) ao portal do cliente!</h2>
      </div>
    );
  }  
}