import axios from "axios";
export default class Api {
    constructor() {
        this.url = 'http://localhost:8080';
    }

    // Cliente
    salvarCliente( cliente ) {
        return axios.post( `${ this.url }/api/cliente/salvar`, cliente );
    }

    buscarClientePorId( id ) {
        return axios.get( `${ this.url }/api/cliente/${ id }` ).then( res => res.data );
    }

    buscarTodosClientes() {
        return axios.get( `${ this.url }/api/cliente/` ).then( res => res.data );
    }

    // Espaço
    salvarEspaco( espaco ) {
        return axios.post( `${ this.url }/api/espaco/salvar`, espaco );
    }

    buscarEspacoPorId( id ) {
        return axios.get( `${ this.url }/api/espaco/${ id }` ).then( res => res.data );
    }

    buscarTodosEspacos() {
        return axios.get( `${ this.url }/api/espaco/` ).then( res => res.data );
    }

    // Pacote
    salvarPacote( pacote ) {
        return axios.post( `${ this.url }/api/pacote/salvar`, pacote );
    }

    buscarPacotePorId( id ) {
        return axios.get( `${ this.url }/api/pacote/${ id }` ).then( res => res.data );
    }

    buscarTodosPacotes() {
        return axios.get( `${ this.url }/api/pacote/` ).then( res => res.data );
    }

    // Contratação
    salvarContratacao( contratacao ) {
        return axios.post( `${ this.url }/api/contratacao/salvar`, contratacao );
    }

    buscarContratacaoPorId( id ) {
        return axios.get( `${ this.url }/api/contratacao/${ id }` ).then( res => res.data );
    }

    buscarTodasContratacoes() {
        return axios.get( `${ this.url }/api/contratacao/` ).then( res => res.data );
    }

    // Pagamento
    realizarPagamento( pagamento ) {
        return axios.post( `${ this.url }/api/pagamento/pagar`, pagamento );
    }

    buscarPagamentoPorId( id ) {
        return axios.get( `${ this.url }/api/pagamento/${ id }` ).then( res => res.data );
    }

    buscarTodosPagamentos() {
        return axios.get( `${ this.url }/api/pagamento/` ).then( res => res.data );
    }

    // Cliente Pacote
    salvarClientePacote( clientePacote ) {
        return axios.post( `${ this.url }/api/clientePacote/salvar`, clientePacote );
    }

    buscarClientePacotePorId( id ) {
        return axios.get( `${ this.url }/api/clientePacote/${ id }` ).then( res => res.data );
    }

    buscarTodosClientePacotes() {
        return axios.get( `${ this.url }/api/clientePacote/` ).then( res => res.data );
    }

    // Espaço Pacote
    salvarEspacoPacote( espacoPacote ) {
        return axios.post( `${ this.url }/api/espacoPacote/salvar`, espacoPacote );
    }

    buscarEspacoPacotePorId( id ) {
        return axios.get( `${ this.url }/api/espacoPacote/${ id }` ).then( res => res.data );
    }

    buscarTodosEspacoPacotes() {
        return axios.get( `${ this.url }/api/espacoPacote/` ).then( res => res.data );
    }
}