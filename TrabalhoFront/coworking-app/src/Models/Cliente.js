export default class Cliente {
    constructor({ nome, cpf, dataNascimento, email, telefone }){
      this.nome = nome;
      this.cpf = cpf;
      this.dataNascimento = dataNascimento;
      this.contatos = [ { tipoContato: { tipoDeContato: "email" }, valor: email }, { tipoContato: { nome: "telefone" }, valor: telefone } ];
    }
  }