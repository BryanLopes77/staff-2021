import React from 'react';
import ReactDOM from 'react-dom';
import Rotas from './Containers/Rotas';

ReactDOM.render( <Rotas />, document.getElementById('root') );

